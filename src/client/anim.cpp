#include "anim.hpp"
#include "imgui.h"
#include "imgui_vals.hpp"
#include <utility>

ImVec2 Animations::center = ImVec2(0,0);
float Animations::symbol_width_half = 0;
int Animations::symb_thickness = 0;

Animations::Animations(): running(false){
    anim_start = std::chrono::system_clock::now();
    last_frame_start = anim_start;
}
void Animations::start(){
    running = true;
    anim_start = std::chrono::system_clock::now();
    last_frame_start = anim_start;
    count = -1;
}

void Animations::update(){
    center = ourPos::cn_out;
    symbol_width_half = ourSizes::outerBoard/3.;
    symb_thickness = ourSizes::cellSize/3.;
}

CrossWinAnim::CrossWinAnim(): rotating(true){
    Animations::update();
    rad_inc = (float)symb_thickness/N_frames;
    double cosa = std::cos(2.*PI/N_frames);
    double sina = std::sin(2.*PI/N_frames);
    points = std::array<ImVec2, N_frames>();
    points[0] = ImVec2(1., 0);
    for(int i = 1; i < N_frames; i++){
        double prv_x = points[i-1].x;
        double prv_y = points[i-1].y;
        double x = prv_x * cosa - prv_y * sina;
        double y = prv_x * sina + prv_y * cosa;
        points[i] = ImVec2(x, y);
    }
    dist_corners = N_frames/4;
    tr_ind = dist_corners/2;
    symbol_diag_half = std::sqrt(2)*symbol_width_half;
    diag_per_2count = symbol_diag_half/(2*N_frames);
    count = -1;
}

void CrossWinAnim::draw_cross(ImDrawList* draw, ImU32 col){
    draw->AddLine(points[(tr_ind + count)%N_frames]*current_symbol_diag_half + center,
     points[(tr_ind + 2*dist_corners + count)%N_frames]*current_symbol_diag_half + center, col, symb_thickness);
    draw->AddLine(points[(tr_ind + dist_corners + count)%N_frames]*current_symbol_diag_half + center,
     points[(tr_ind + 3*dist_corners + count)%N_frames]*current_symbol_diag_half + center, col, symb_thickness);
}
void CrossWinAnim::update(){
    Animations::update();
    symbol_diag_half = std::sqrt(2)*symbol_width_half;
    diag_per_2count = symbol_diag_half/(2*N_frames);

}

bool CrossWinAnim::goon(ImDrawList* draw, bool should_run){

    if(running && should_run){
        TimePoint now = std::chrono::system_clock::now();
        if(count == -1){
            anim_start = now;
            count++;
        }
        float dt = std::chrono::duration_cast<std::chrono::milliseconds>(now - anim_start).count();
        current_symbol_diag_half = diag_per_2count*count;
        int c = dt/dt_ms;
        if(count < N_frames*2){
            if(c > count){ 
                this->count = c;
                
            }
            this->draw_cross(draw, getCol(ourColors::Cross));
            return true;
            
        }
        else{
            running = false;
            return false;
            
        }
        return true;
    }else if(should_run){
        draw->AddLine(ImVec2(-1,-1)*symbol_diag_half + center,
        ImVec2(1,1)*symbol_diag_half + center, getCol(ourColors::Cross), symb_thickness);
        draw->AddLine(ImVec2(1,-1)*symbol_diag_half + center,
        ImVec2(-1,1)*symbol_diag_half + center, getCol(ourColors::Cross), symb_thickness);
        return false;
    }
    return false;
}

CircleWinAnim::CircleWinAnim(){
    Animations::update();
    rad_inc = (float)symb_thickness/N_circles;
    double cosa = std::cos(2.*PI/N_circles);
    double sina = std::sin(2.*PI/N_circles);
    small_circles = std::array<ImVec2, N_circles>();
    rad_small_circles = std::array<float, N_circles>();
    small_circles[0] = ImVec2(1., 0.);
    rad_small_circles[0] = 0.;
    for(int i = 1; i < N_circles; i++){
        double prv_x = small_circles[i-1].x;
        double prv_y = small_circles[i-1].y;
        double x = prv_x * cosa - prv_y * sina;
        double y = prv_x * sina + prv_y * cosa;
        small_circles[i] = ImVec2(x, y);
        rad_small_circles[i] = rad_small_circles[i-1] + rad_inc;
    }
    count = -1;
}

void CircleWinAnim::draw_circle(ImDrawList* draw, ImU32 col){
    for(int i = 0; i < N_circles && i < count; i++){
        if(i <= count- N_circles){
            draw->AddCircleFilled(small_circles[i]*symbol_width_half + center, symb_thickness, col);
        }
        else{
            draw->AddCircle(small_circles[i]*symbol_width_half + center, rad_small_circles[count - i], col);
        }
    }
}
void CircleWinAnim::update(){
    Animations::update();
    rad_inc = (float)symb_thickness/N_circles;
    float r = 0.;
    for(int i = 1; i < N_circles; i++){
        rad_small_circles[0] = r;
        r += rad_inc;
    }

}
bool CircleWinAnim::goon(ImDrawList* draw, bool should_run){
    if(running && should_run){
        TimePoint now = std::chrono::system_clock::now();
        if(count == -1){
            anim_start = now;
            running = true;
            count++;
        }
        float dt = std::chrono::duration_cast<std::chrono::milliseconds>(now - anim_start).count();
        int c = dt/dt_ms;
        if(count < N_circles*2){
            if(c > count){ 
                this->count = c;
            }
            this->draw_circle(draw, getCol(ourColors::Circle));
            return true;
        }
        else{
            running = false;
            return false;
            
        }
        return true;
    }else if(should_run){
        draw->AddCircle(center, symbol_width_half, getCol(ourColors::Circle), 0, 2*symb_thickness);
        return false;
    }
    return false;
}


GameOverAnim::GameOverAnim(){
    Animations::update();
    if(ourFonts::GameOver != nullptr) {cur_font_scale = ourFonts::GameOver->Scale;}
    else{cur_font_scale = 1.;}
    int size = 2*ourSizes::cellSize;

    game_sz_end = ImVec2(2.5*size, size);
    over_sz_end = ImVec2(2.25*size, size);
    game_tl_end = ImVec2(center.x - 0.5*game_sz_end.x,ourPos::cell_centers[11].y);
    over_tl_end = ImVec2(center.x - 0.5*over_sz_end.x, ourPos::cell_centers[38].y);
    tl_start = ImVec2(0,0);
    sz_start = ImVec2(ourSizes::window_width, ourSizes::window_height);
    game_inc_tl = (game_tl_end - tl_start)/N_frames;
    game_inc_sz = (game_sz_end - sz_start)/N_frames;
    over_inc_tl = (over_tl_end - tl_start)/N_frames;
    over_inc_sz = (over_sz_end - sz_start)/N_frames;
    continue_sz = ImVec2(9*ourSizes::thinFont, 3*ourSizes::thinFont);
    continue_tl = ImVec2(center.x-0.5*continue_sz.x, ourPos::cell_corners[76].first.y);
    count = -1;
}

bool GameOverAnim::goon(ImDrawList *draw_list, bool should_run){    
    if(running && should_run){
        TimePoint now = std::chrono::system_clock::now();
        if(count == -1){
            anim_start = now;
            running = true;
            cur_sz = sz_start;
            count++;
        }
        float dt = std::chrono::duration_cast<std::chrono::milliseconds>(now - anim_start).count();
        this->count = dt/dt_ms;
        ImGui::SetNextWindowPos(continue_tl);
        ImGui::SetNextWindowSize(continue_sz);
        ImGui::Begin("continue", &(this->running), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
        ImGui::PushFont(ourFonts::thin);
        float x_diff = ImGui::GetCursorScreenPos().x - continue_tl.x;
        float y_diff = ImGui::GetCursorScreenPos().y - continue_tl.y;
        if(ImGui::Button("Continue", continue_sz- ImVec2(2*x_diff, 2*y_diff))){
            running = false;
            ImGui::PopFont();
            ImGui::End();
            return false;
        }
        
        ImGui::PopFont();
        ImGui::End();
        ImGui::PushStyleColor(ImGuiCol_WindowBg, ImGui::GetColorU32(ourColors::black));
        if(count < N_frames){
           
            cur_sz = sz_start + game_inc_sz*count;
            ourFonts::GameOver->Scale = cur_font_scale*cur_sz.x/game_sz_end.x;
            ImGui::PushFont(ourFonts::GameOver);

            ImGui::SetNextWindowPos(tl_start + game_inc_tl*count);
            ImGui::SetNextWindowSize(cur_sz);
            ImGui::Begin("game", &(this->running), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
            ImGui::TextColored(ourColors::Cross,"GAME");
            ImGui::End();
            ImGui::PopFont();
            ImGui::PopStyleColor();
            ourFonts::GameOver->Scale = cur_font_scale;
            return true;
        }else if(count < 2*N_frames){
            ImGui::PushFont(ourFonts::GameOver);
            ImGui::SetNextWindowPos(game_tl_end);
            ImGui::SetNextWindowSize(game_sz_end);
            ImGui::Begin("game", &(this->running), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
            ImGui::TextColored(ourColors::Cross,"GAME");
            ImGui::End();
            ImGui::PopFont();
            cur_sz = sz_start + over_inc_sz*(count-N_frames);
            ourFonts::GameOver->Scale = cur_font_scale*cur_sz.x/game_sz_end.x;
            ImGui::PushFont(ourFonts::GameOver);
            ImGui::SetNextWindowPos(tl_start + over_inc_tl*(count - N_frames));
            ImGui::SetNextWindowSize(sz_start + over_inc_sz*(count - N_frames));
            ImGui::Begin("over", &(this->running), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
            ImGui::TextColored(ourColors::Cross,"OVER");

            ImGui::End();
            ImGui::PopFont();
            ImGui::PopStyleColor();
            ourFonts::GameOver->Scale = cur_font_scale;

            return true;
        }
        else{
            ImGui::PushFont(ourFonts::GameOver);

            ImGui::SetNextWindowPos(game_tl_end);
            ImGui::SetNextWindowSize(game_sz_end);
            ImGui::Begin("game", &(this->running), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
            ImGui::TextColored(ourColors::Cross,"GAME");
            ImGui::End();
            ImGui::SetNextWindowPos(over_tl_end);
            ImGui::SetNextWindowSize(over_sz_end);
            ImGui::Begin("over", &(this->running), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
            ImGui::TextColored(ourColors::Cross,"OVER");
            
            ImGui::End();
            ImGui::PopFont();
            ImGui::PopStyleColor();
            return true;
            
        }
        return true;
    }else if(should_run){

        return false;
    }

    return false;
}
void GameOverAnim::update(){
    Animations::update();
    cur_font_scale = ourFonts::GameOver->Scale;
    int size = 2*ourSizes::cellSize;
    game_sz_end = ImVec2(2.5*size, size);
    over_sz_end = ImVec2(2.25*size, size);
    game_tl_end = ImVec2(center.x - 0.5*game_sz_end.x,ourPos::cell_centers[11].y);
    over_tl_end = ImVec2(center.x - 0.5*over_sz_end.x, ourPos::cell_centers[38].y);
    tl_start = ImVec2(0,0);
    sz_start = ImVec2(ourSizes::window_width, ourSizes::window_height);
    game_inc_tl = (game_tl_end - tl_start)/N_frames;
    game_inc_sz = (game_sz_end - sz_start)/N_frames;
    over_inc_tl = (over_tl_end - tl_start)/N_frames;
    over_inc_sz = (over_sz_end - sz_start)/N_frames;
    continue_sz = ImVec2(9*ourSizes::thinFont, 3*ourSizes::thinFont);
    continue_tl = ImVec2(center.x-0.5*continue_sz.x, ourPos::cell_corners[76].first.y);
}
bool GameOverAnim::started(){
    return running;
}
