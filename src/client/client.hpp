#ifndef UTTT_CLIENT_HPP
#define UTTT_CLIENT_HPP

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
//sockpp deps
#include <sockpp/tcp_connector.h>
#include <sockpp/tcp_socket.h>
//common deps
#include "../common/message.hpp"
#include "../common/common.hpp"
#include "../common/game.h"
//server deps
#include "../server/server.hpp"

//client deps
#include "anim.hpp"

//std deps
#include <stdio.h>
#include <cstdlib>
#include <thread>
#include <variant>
#include <queue>
#include <chrono>
//imgui deps
#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
//#include "imgui_vals.hpp"

#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h> // Will drag system OpenGL headers

/*
 include order: imgui_vals.hpp -> helpers.hpp -> anim.hpp -> client.hpp
*/

//probably should not be defined here
enum class ChangeTo {
  MenuSelection,      //only for development
  StartMenu,          //initial screen
  ConnectionScreen,   //waiting before gamestart
  WaitScreen,         //waiting for other player
  JoinMenu,           //serverID input
  HostMenu,           //Choose mode and such
  RunMainGame,        //game is running
  GameOverMenu,       //game is over
  Quit,               //close application
  Error,              //shouldn't happen
  RunLocalGame
};

class Client {
    public:
        //starts glfw window and imgui context
        Client();
        ~Client();
        int run();
    private:

        using IntRetGui = std::function<int()>;
        using VoidRetGui = std::function<void()>;
        using ChangeToRetGui = std::function<ChangeTo()>;
        //using TimePoint = std::chrono::time_point<std::chrono::system_clock>;

        sockpp::tcp_connector _connector;
        ChangeTo menu_selection();
        ChangeToRetGui *menu_selection_ptr;

        bool try_play(const GenericPos &outer, const GenericPos &inner);
        /*
          start_menu(): initial frame for users; lets them choose to connect or host
            * lets user choose username
            * returns Start without selection. 
            * returns {Exit, Connect, Host} according to the users selection.
        */
        ChangeTo start_menu();
        ChangeToRetGui *start_menu_ptr;
        /*
          host_menu(): lets user choose game mode, set up server.

        */
        ChangeTo host_menu();
        ChangeToRetGui *host_menu_ptr;
        /*
          join_menu(): lets user input serverID, connect to server.
        */
        ChangeTo join_menu();
        ChangeToRetGui *join_menu_ptr;
        /*
          end_game_menu(): lets user choose to play again or quit.
        */
        ChangeTo game_over_menu();
        ChangeToRetGui *game_over_menu_ptr;
        /*
          connection_screen(): lets user know that they are waiting for a connection.
        */
        ChangeTo ConnectionScreen();
        ChangeToRetGui *connection_screen_ptr;
        /*
          game_screen(): lets user play the game.
        */
        ChangeTo run_main_game();        
        ChangeToRetGui *game_screen_ptr;
        ChangeTo run_local_main_game();
        ChangeToRetGui *local_game_screen_ptr;
        /*
          wait_screen(): lets user know that they are waiting for something
        */
        ChangeTo wait_for_other();
        ChangeToRetGui *wait_screen_ptr;

        ChangeTo error_screen();
        ChangeToRetGui *error_screen_ptr;

        void listen_for_msg(bool *should_run);
        void handle_msg();

        bool connect();
        void send_move_to_server(NextMove&);
        ServerMessage get_server_response();

        int light_or_darkmode();
        IntRetGui *light_dark;
        bool dark_mode = true;
  
        int scoreboard();
        IntRetGui *scbrd;
        void draw_tile(const GenericPos &out, const GenericPos &inner,  bool on_active_board);
        void draw_inner_board(const GenericPos &out);

        void create_drawdata_inactive();

        void begin_of_frame();
        void end_of_frame(GLFWwindow *window, ImVec4& clear_color, bool show_scoreboard) ;

        

        template<typename ret, typename func>
        ret display_on_fixed_window(ImVec2 tl, ImVec2 br, func *what_to_display, ImU32 bg_col = ImGui::GetColorU32(ourColors::bg)){
          IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
          ImGui::PushStyleColor(ImGuiCol_WindowBg, bg_col);
          ImGui::SetNextWindowPos(tl);
          ImGui::SetNextWindowSize(ImVec2(br.x - tl.x, br.y - tl.y));
          ImGui::Begin("fixed window", &(this->isOpen), ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
          this->draw_window = ImGui::GetWindowDrawList();
          ret res = (*what_to_display)();
          ImGui::End();
          ImGui::PopStyleColor();
          return res;
        };

        //imgui stuff
        GLFWwindow* window;
        ImDrawList* board;      //gameboard -> i think its possible to store content, so we dont have to calclulate it every frame
        ImDrawList* draw_bg;    //background and gameboard
        ImDrawList* draw_window;//everything after "IMGUI_Begin()-> update after each call!"

        ImGuiStyle *style;
        ImGuiIO io;
        //game stuff -> probably not everything nessesary
        unsigned games_started;
        bool isOpen;

        //values to adjust to frame size
        inline void update_vals(int width, int height);

        //game stuff
        Player my_symbol;
        Player opponent;
        //Player winner;
        GameStateMachine game_state_machine;
        MoveResult last_move_result;

        //animation stuff
        bool anim_running;
        CrossWinAnim CrossWin;
        CircleWinAnim CircleWin;
        GameOverAnim GameOver;
      
        //server stuff
        int port;
        char server_ip[50];
        //message stuff
        ServerMessage last_recieved_msg;
        ClientMessage last_sent_msg;
        bool send_msg;
        //thread stuff ->!!!!I HAVE NO IDEA WHAT IM DOING, PLEASE COME BACK TO THIS!!!!
        std::thread *listen_thread;
        std::thread *server_thread;
        //std::thread *handle_thread;
        bool server_running_locally;
        bool server_connected;
        bool tried_to_connect;
        bool GimmeGimmenewmsg;//to indicate, that listener needs to wait until Client has reacted to result of last msg.
    
};

#undef DRAW
#define DRAW
struct PrePlayedGames{
#ifdef DRAW
  static constexpr std::array<PlaceSymbolProposal, 35> game
        = { PlaceSymbolProposal{0,6}, PlaceSymbolProposal{6,0}, PlaceSymbolProposal{0,7}, PlaceSymbolProposal{7,0},
            PlaceSymbolProposal{0,8}, PlaceSymbolProposal{8,1}, PlaceSymbolProposal{1,7}, PlaceSymbolProposal{7,1},
            PlaceSymbolProposal{1,6}, PlaceSymbolProposal{6,1}, PlaceSymbolProposal{1,8}, PlaceSymbolProposal{8,2},
            PlaceSymbolProposal{2,8}, PlaceSymbolProposal{8,0}, PlaceSymbolProposal{2,7}, PlaceSymbolProposal{7,2},
            PlaceSymbolProposal{2,2}, PlaceSymbolProposal{2,6}, PlaceSymbolProposal{6,2}, PlaceSymbolProposal{2,5},
            PlaceSymbolProposal{5,2}, PlaceSymbolProposal{2,4}, PlaceSymbolProposal{4,2}, PlaceSymbolProposal{2,3},
            PlaceSymbolProposal{3,6}, PlaceSymbolProposal{6,8}, PlaceSymbolProposal{6,4}, PlaceSymbolProposal{4,6},
            PlaceSymbolProposal{6,6}, PlaceSymbolProposal{3,8}, PlaceSymbolProposal{5,6}, PlaceSymbolProposal{3,5},
            PlaceSymbolProposal{5,3}, PlaceSymbolProposal{3,2}, PlaceSymbolProposal{5,0}};
#endif
    static Player me;
    static int count;

    static void init(Player mysymb, Player beginner){ 
      PrePlayedGames::me = mysymb;
      PrePlayedGames::count = (unsigned)(mysymb != beginner);
    }
    static NextMove my_move(){
      if(count < 35){
        GenericPos board = GenericPos::fromLinearIdx(PrePlayedGames::game[count].inner_board_index);
        GenericPos tile = GenericPos::fromLinearIdx(PrePlayedGames::game[count].tile_index);
        count += 2;
        return NextMove(me, PosIter({board, tile}));
      }
      return NextMove(me, PosIter({GenericPos(0,0), GenericPos(0,0)}));
    }
};

#endif //UTTT_CLIENT_HPP
