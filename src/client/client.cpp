#include "client.hpp"
#include "imgui_vals.hpp"
#include <iostream>
#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "imgui.h"

#include <variant>


/*
    MODE <  5: server runs
    and...{
        MODE >= 1: prints messages
        MODE >= 2: prints thread related stuff
        MODE >= 3: game logic stuff (mainly indices)
        Mode == 4: play preplayed game-> use struct played_game
        }
    MODE >= 5: server does not run
*/

#undef MODE
#define MODE 0

#define SMALL_PADDING ImGui::Dummy(ImVec2(20, 20))
#define BIG_PADDING ImGui::Dummy(ImVec2(50, 50))

int PrePlayedGames::count = 0;
Player PrePlayedGames::me = Player::Cross;

void TextCentered(std::string text) {
    auto windowWidth = ImGui::GetWindowSize().x;
    auto textWidth   = ImGui::CalcTextSize(text.c_str()).x;

    ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
    ImGui::Text("%s", text.c_str());
}

bool ButtonCentered(std::string text) {
    auto windowWidth = ImGui::GetWindowSize().x;
    auto textWidth   = ImGui::CalcTextSize(text.c_str()).x;

    ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
    return ImGui::Button(text.c_str());
}

ChangeTo Client::menu_selection() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::bold);
    
    TextCentered("Welcome to our menu selection!");
    TextCentered("Please select a menu below, \n and send your remarks to the client devs:");
    if (ButtonCentered("Host Game")) {

        ImGui::PopFont();
        return ChangeTo::HostMenu;
    }
    if (ButtonCentered("Join Game")) {

        ImGui::PopFont();
        return ChangeTo::JoinMenu;
    }
    if (ButtonCentered("start menu")) {

        ImGui::PopFont();
        return ChangeTo::StartMenu;
    }
    if (ButtonCentered("main game")) {
        ImGui::PopFont();
        this->game_state_machine = GameStateMachine(Circle);
        this->my_symbol = Player::Circle;
        this->opponent = Player::Cross;
        this->game_state_machine.start_new_game();
        return ChangeTo::RunMainGame;
    }
    if (ButtonCentered("main game over")) {
        ImGui::PopFont();
        return ChangeTo::GameOverMenu;
    }
    if (ButtonCentered("Connect")) {
        ImGui::PopFont();
        return ChangeTo::ConnectionScreen;
    }
    // if (ButtonCentered("Exit")) {
    //     ImGui::PopFont();
    //     return ChangeTo::Quit;
    // }
    ImGui::PopFont();
    
    return ChangeTo::MenuSelection;
}

/*
    Client::start_menu()
           user actions  | ToDo
        1. Host game     | call host_menu()
        2. Join game     | call join_menu()
        3. Quit          | Close application
*/

ChangeTo Client::start_menu() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::symbol);
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    
    light_or_darkmode();
    
    TextCentered("Welcome to Ultimate Tic Tac Toe!");
    ImGui::PopFont();
    ImGui::PushFont(ourFonts::bold);


    TextCentered("Here you can choose what you want to do:");
    // TextCentered("Do you want to connect to a server, or start one locally?:");
    // TextCentered("TODO: make it look nicer\nassigned to: ");
    SMALL_PADDING;
    if (ButtonCentered("Start a server")) {

        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::HostMenu;
    }
    SMALL_PADDING;
    if (ButtonCentered("Connect to a server")) {

        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::JoinMenu;
    }
    SMALL_PADDING;
    if (ButtonCentered("Run a local game")) {

        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::RunLocalGame;
    }
    #if MODE >= 5
    SMALL_PADDING;
    if (ButtonCentered("Exit")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::Quit;
    }
    SMALL_PADDING;
    if (ButtonCentered("back to selection")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::MenuSelection;
    }
    #endif
    ImGui::PopFont();
    ImGui::PopStyleColor();
    
    
    return ChangeTo::StartMenu;
}
/*
    Client::host_menu()
            user actions        | ToDo
         1. Start server        | start server, allow user to connect
         2. Connect to server   | connect to server, call wait_for_other()
         3. Quit                | Close application
*/

ChangeTo Client::host_menu() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::bold);
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    light_or_darkmode();
    if(this->server_running_locally){

        #if MODE < 5
            connect();
        #endif
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::WaitScreen;
        TextCentered("The server should be ready now. whenever you are, hit connect");
        if (ButtonCentered("Connect")) {
        #if MODE < 5
            connect();
        #endif
            ImGui::PopFont();
            ImGui::PopStyleColor();
            return ChangeTo::WaitScreen;
        }
        #if MODE >= 5
        if (ButtonCentered("Exit")) {
            ImGui::PopFont();
            ImGui::PopStyleColor();
            ////ImGui::End();
            return ChangeTo::Quit;
        }
        TextCentered("###########################BELOW IS ONLY FOR DEVELOPING###########################");
        if (ButtonCentered("back to selection")) {
            ImGui::PopFont();
            ImGui::PopStyleColor();
            return ChangeTo::MenuSelection;
        }
            TextCentered("In this window, you can start the server\nand do some game settings.\n"
                    "evtl input the username\n"
                    "assigned to: ");
        #endif
    }else{
        TextCentered("Please tell us where the server should run.");
        TextCentered("If you want to run the server on");
        TextCentered("this machine with default settings,");
        TextCentered("you can just hit \"Start Server\"");
        SMALL_PADDING;
        ImGui::Text(" Server IPv4: ");
        ImGui::SameLine();
        auto x_position = ImGui::GetCursorScreenPos().x;
        ImGui::InputText("##1", this->server_ip, 50);
        ImGui::Text(" Server Port: ");
        ImGui::SameLine();
        // align the two boxes
        ImGui::SetCursorPosX(x_position);
        ImGui::InputInt("##2", &(this->port));
        SMALL_PADDING;
        if(ButtonCentered("Start Server")){
            #if MODE < 5
            std::string serverIPstring = std::string(this->server_ip);
            int p = this->port;
            std::thread serverer([serverIPstring, p](){
                std::string IPv4 = serverIPstring;
                Server server;
                server.run_with_Ip_port(IPv4, p);
            });
            serverer.detach();
            this->server_thread = &serverer;
            this->server_running_locally = true;
            #endif
        }
        SMALL_PADDING;
        TextCentered("Here you can go back to the start menu:");
        SMALL_PADDING;
        if (ButtonCentered("start menu")) {
            ImGui::PopFont();
            ImGui::PopStyleColor();
            return ChangeTo::StartMenu;
        }
        

    }
    ImGui::PopFont();
    ImGui::PopStyleColor();
    return ChangeTo::HostMenu;
}

/*
    Client::join_menu()
            user actions        | ToDo
         1. Connect to server   | connect to server, wait for gamestart msg, then call run_main_game()
         2. Quit                | Close application
    preconditions: server is running 
    
*/

ChangeTo Client::join_menu() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::bold);
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    light_or_darkmode();
    TextCentered("Please enter the address of the server you want to connect to.");
    TextCentered("If you want to connect to a server running");
    TextCentered("on the same machine with default settings,");
    TextCentered("you can just hit \"Connect\"");
    SMALL_PADDING;
    ImGui::Text(" Server IPv4: ");
    ImGui::SameLine();
    auto x_position = ImGui::GetCursorScreenPos().x;
    ImGui::InputText("##1", this->server_ip, 50);
    ImGui::Text(" Server Port: ");
    ImGui::SameLine();
    // align the two boxes
    ImGui::SetCursorPosX(x_position);
    ImGui::InputInt("##2", &(this->port));
    SMALL_PADDING;
    if (!this->server_connected && ButtonCentered("Connect")) {
 #if MODE < 5
        this->server_connected = connect();
        this->tried_to_connect = true;
 #endif
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::JoinMenu;
    }
    if(this->tried_to_connect && !this->server_connected){
        SMALL_PADDING;
        ImGui::PushFont(ourFonts::thin);
        TextCentered("Failed to connect to server! Please check the address and port");
        TextCentered("and make sure the server is running. Then try again.");
        ImGui::PopFont();
    }
    if( std::holds_alternative<GameStart>(this->last_recieved_msg.specific)){
        this->GimmeGimmenewmsg = true;
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::RunMainGame;
    } else {
        SMALL_PADDING;
        TextCentered("Here you can go back to the start menu:");
        SMALL_PADDING;
        if (ButtonCentered("start menu")) {
            ImGui::PopFont();
            ImGui::PopStyleColor();
            return ChangeTo::StartMenu;
        }
    }
    
    #if MODE >= 5
    TextCentered("###########################BELOW IS ONLY FOR DEVELOPING###########################");
    TextCentered(
            "In this window, you can join a game. \nFor that, another person has to already \nhave startet host_menu.\n"
            "otherwise the connection can't be done.\n"
            "evtl input the username.\n"
            "assigned to: ");
    if (ButtonCentered("back to selection")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::MenuSelection;
    }
    #endif
    ImGui::PopFont();
    ImGui::PopStyleColor();
    return ChangeTo::JoinMenu;
}


/*
    Client::wait_for_other()
            user actions        | condition to allow        | ToDo
         1. Start game          | recieved gamestart msg    | call run_main_game(), allow new messages
         2. Quit                | -                         | Close application, send message to server


*/
ChangeTo Client::wait_for_other() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::bold);
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    light_or_darkmode();
    TextCentered("The server and you are ready.");
    TextCentered(" Your opponent needs the following information to connect,");
    TextCentered("please let them know:");
    TextCentered("Server address (IPv4:port): " + this->_connector.peer_address().to_string());
    
        if(std::holds_alternative<GameStart>(this->last_recieved_msg.specific)){
            this->GimmeGimmenewmsg = true;
            ImGui::PopFont();
            ImGui::PopStyleColor();
            return ChangeTo::RunMainGame;
        }
    #if MODE >= 5
    SMALL_PADDING;
    if (ButtonCentered("Exit")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::Quit;
    }
    if (ButtonCentered("back to selection")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::MenuSelection;
    }
    #endif
    ImGui::PopFont();
    ImGui::PopStyleColor();
    return ChangeTo::WaitScreen;
}

ChangeTo Client::ConnectionScreen() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::thin);
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    TextCentered("this can probably be deleted later, just add some visual stuff to show\n"
                "that the connection works in host_window and join_game_window");
    #if MODE >= 5
    if (ButtonCentered("back to selection")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::MenuSelection;
    }
    #endif
    ImGui::PopFont();
    ImGui::PopStyleColor();
    return ChangeTo::ConnectionScreen;
}

/*
    Client::run_main_game()
        user actions        | condition to allow        | ToDo
     1. Place symbol        | gamestate allows          | update gamestate, send message to server
     2. Resign              | gamestate allows          | update gamestate, send message to server
     3. Go to Game Over menu| End of game anim over     | call game_over_menu(), allow new messages
     3. Quit                | always                    | Close application, send message to server

        opponent actions    | can happen when...        | ToDo                                  | Done by
     1. Place symbol        | not my turn               | update gamestate, allow new msg       | handle_msg()
     2. Resign              | not my turn               | update gamestate                      | handle_msg()
     3. Quit                | always                    | -> what ?                             | handle_msg()

        Gamestates          | ToDo                  | Done by
     1. in progress         | place symbols         | handle_msg()
                            | send moves            | send_move_to_server()
                            | allow resignation     | run_main_game()
     2. finished, both clients connected
                            | show win anim         | run_main_game()
                            | continue button       | run_main_game()
     3. finished, opponent left
                            | what?                 | run_main_game()
                            |call game_over_menu()  | run_main_game()
*/
ChangeTo Client::run_main_game(){
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    
    //std::string addr = this->_connector.peer_address().to_string();
    //if(this->server_connected && !addr.compare("0.0.0.0:0")){
    //    this->server_connected = false;
    //    std::cout << "server disconnected" << std::endl;
    //    this->game_state_machine.resign(this->opponent);
    //    this->last_recieved_msg.specific = OpponentLeft{};
    //}
        
    #if MODE >= 5
        if(this->my_symbol != game_state_machine.data_->currentPlayer){
            this->my_symbol = game_state_machine.data_->currentPlayer;
        }
    #endif
     #if MODE == 4
        if(this->my_symbol == game_state_machine.data_->currentPlayer && this->game_state_machine.state_ == GameState::InProgress){
            auto mv = PrePlayedGames::my_move();
            this->send_move_to_server(mv);        
        }
    #endif
    this->draw_bg = ImGui::GetBackgroundDrawList();
    
    
    this->draw_bg->ChannelsSplit(2);
    for(int yo = 0; yo < 3; yo++){
        for(int xo = 0; xo < 3; xo++){
            const GenericPos out = GenericPos(xo, yo);
            draw_inner_board(out);
        }
    }
    if(this->game_state_machine.state_ == GameState::Finished){
        bool win_anim = true;
        TileBoardState state = this->game_state_machine.data_->board->state_as_board();
        if( std::holds_alternative<TileBoardStateWon>(state)){
            
            Player winner = std::get<TileBoardStateWon>(state).player;
            if(winner == Player::Cross){
                if(!this->anim_running){
                    this->anim_running = true;
                    this->CrossWin.start();
                    this->GameOver.start();
                }
                else{
                    this->draw_bg->ChannelsSetCurrent(1);
                    win_anim = CrossWin.goon(this->draw_bg, this->anim_running);
                    this->draw_bg->ChannelsSetCurrent(0);
                }
            }
            else if(winner == Player::Circle){
                if(!this->anim_running){
                    this->anim_running = true;
                    this->GameOver.start();
                    this->CircleWin.start();
                }
                else{
                    this->draw_bg->ChannelsSetCurrent(1);
                    win_anim = CircleWin.goon(this->draw_bg, this->anim_running);
                    this->draw_bg->ChannelsSetCurrent(0);
                    
                }
            }
        }else if(std::holds_alternative<TileBoardStateDraw>(state)){
            win_anim = false;
            if(!this->anim_running){
                this->anim_running = true;
                this->GameOver.start();
            }
        }
        if(!win_anim && !GameOver.goon(this->draw_bg, this->anim_running)){
            this->anim_running = false;
            this->GimmeGimmenewmsg = true;
            return ChangeTo::GameOverMenu;
        }
    }
    
    this->draw_bg->ChannelsMerge();
    
    return ChangeTo::RunMainGame;
}

ChangeTo Client::run_local_main_game(){
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    if(this->my_symbol != game_state_machine.data_->currentPlayer){
            this->my_symbol = game_state_machine.data_->currentPlayer;
    }
    this->draw_bg = ImGui::GetBackgroundDrawList();
   
    
    this->draw_bg->ChannelsSplit(2);
    for(int yo = 0; yo < 3; yo++){
        for(int xo = 0; xo < 3; xo++){
            const GenericPos out = GenericPos(xo, yo);
            draw_inner_board(out);
        }
    }
    if(this->game_state_machine.state_ == GameState::Finished){
        bool win_anim = true;
        TileBoardState state = this->game_state_machine.data_->board->state_as_board();
        if( std::holds_alternative<TileBoardStateWon>(state)){
            
            Player winner = std::get<TileBoardStateWon>(state).player;
            if(winner == Player::Cross){
                if(!this->anim_running){
                    this->anim_running = true;
                    this->CrossWin.start();
                    this->GameOver.start();
                }
                else{
                    this->draw_bg->ChannelsSetCurrent(1);
                    win_anim = CrossWin.goon(this->draw_bg, this->anim_running);
                    this->draw_bg->ChannelsSetCurrent(0);
                }
            }
            else if(winner == Player::Circle){
                if(!this->anim_running){
                    this->anim_running = true;
                    this->GameOver.start();
                    this->CircleWin.start();
                }
                else{
                    this->draw_bg->ChannelsSetCurrent(1);
                    win_anim = CircleWin.goon(this->draw_bg, this->anim_running);
                    this->draw_bg->ChannelsSetCurrent(0);
                    
                }
            }
        }
        if(!win_anim && !GameOver.goon(this->draw_bg, this->anim_running)){
            this->anim_running = false;
            this->GimmeGimmenewmsg = true;
            return ChangeTo::GameOverMenu;
        }
    }
    
    this->draw_bg->ChannelsMerge();
    
    return ChangeTo::RunLocalGame;
}

ChangeTo Client::game_over_menu() {
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    light_or_darkmode();
    ImGui::PushFont(ourFonts::bold);
    TextCentered("Well Played!");
    ImGui::PopFont();
    ImGui::PushFont(ourFonts::thin);
    TextCentered("The current scores are:");
    int x_wins = this->game_state_machine.stats_.cross_wins;
    int o_wins = this->game_state_machine.stats_.circle_wins;
    if (!this->server_connected) {
        if (x_wins == 0) {
            TextCentered("The Player with the symbol X hasn't won any games yet");
        } else if (x_wins == 1) {
            TextCentered("The Player with the symbol X has won one game");
        } else {
            TextCentered("The Player with the symbol X has won " + std::to_string(x_wins) + " games");
        }
        if (o_wins == 0) {
            TextCentered("The Player with the symbol O hasn't won any games yet");
        } else if (o_wins == 1) {
            TextCentered("The Player with the symbol O has won one game");
        } else {
            TextCentered("The Player with the symbol O has won " + std::to_string(o_wins) + " games");
        }
    } else {
        int my_wins = (this->my_symbol == Player::Circle) ? o_wins : x_wins;
        int other_wins = (this->my_symbol == Player::Cross) ? o_wins : x_wins;
        if (my_wins == 0) {
            TextCentered("You haven't won any games yet");
        } else if (my_wins == 1) {
            TextCentered("You have won one game");
        } else {
            TextCentered("You have won " + std::to_string(my_wins) + " games");
        }
        if (other_wins == 0) {
            TextCentered("Your opponent hasn't won any games yet");
        } else if (other_wins == 1) {
            TextCentered("Your opponent has won one game");
        } else {
            TextCentered("Your opponent has won " + std::to_string(o_wins) + " games");
        }

        
    }
    #if MODE >= 5
    TextCentered(
            "In this menu, you can see the current score \nand the players can decide if they \nwant to play again or go\n"
            "back to the main selection menu\n"
            "assigned to: ");
    
    if (ButtonCentered("back to selection")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::MenuSelection;
    }
    #endif
    if(std::holds_alternative<OpponentLeft>(this->last_recieved_msg.specific)){
        TextCentered("Your opponent left. You can hang out here, or close the app.");
        this->GimmeGimmenewmsg = true;
    }else{
        if(std::holds_alternative<RevancheProposal>(this->last_recieved_msg.specific)){
            SMALL_PADDING;
            TextCentered("Your opponent wants to Play again."); 
            TextCentered("Are you brave enough, or do you want to close the app?");
            SMALL_PADDING;
            if(ButtonCentered("Accept revanche")){
                this->game_state_machine.start_new_game();
                this->games_started++;
                this->last_sent_msg.specific = RevancheAccepted{};
                this->send_msg = true;
                this->GimmeGimmenewmsg = true; 
                ImGui::PopFont();
                ImGui::PopStyleColor();

                return ChangeTo::RunMainGame;
            }
        }else if(!std::holds_alternative<RevancheProposal>(this->last_sent_msg.specific)){
            SMALL_PADDING;
            TextCentered("Here, you can choose if you want to surrender");
            TextCentered("Or if you want improve your stats and play again:");
            SMALL_PADDING;
            if (ButtonCentered("Play again")) {
                //ImGui::PopFont();
                //this->game_state_machine.start_new_game();
                if (this->server_connected) {
                    this->last_sent_msg.specific = RevancheProposal{};
                    this->send_msg = true;
                    this->GimmeGimmenewmsg = true; 
                } else {
                    this->game_state_machine.start_new_game();

                    this->games_started++;
                    ImGui::PopFont();
                    ImGui::PopStyleColor();
                    return ChangeTo::RunLocalGame;
                }
                ImGui::PopFont();
                ImGui::PopStyleColor();
                return ChangeTo::GameOverMenu;
            }
        }else if(std::holds_alternative<RevancheAccepted>(this->last_recieved_msg.specific)){
            //SMALL_PADDING;
            //TextCentered("Your opponent accepted.");
            //TextCentered("did you get cold feet in the meantime?");
            //if(ButtonCentered("no")||ButtonCentered("NO")||
            //   ButtonCentered("no no no no NO")||ButtonCentered("ok, maybe")){
                this->game_state_machine.start_new_game();
                this->games_started++;
                this->GimmeGimmenewmsg = true;
                ImGui::PopFont();
                ImGui::PopStyleColor();
                return ChangeTo::RunMainGame;
            //}
        }
        else{
            SMALL_PADDING;
            TextCentered("Please wait until your opponent decides");
            TextCentered("wether he is brave enough to play again\n");
        }
    }
    // if (ButtonCentered("Exit")) {
    //     ImGui::PopFont();
    //     ImGui::PopStyleColor();
    //     return ChangeTo::Quit;
    // }
    ImGui::PopFont();
    ImGui::PopStyleColor();
    return ChangeTo::GameOverMenu;
}

ChangeTo Client::error_screen(){
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    ImGui::PushFont(ourFonts::thin);
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    TextCentered("Something went wrong. we are trying hard to resolve the issue,\n"
                "but you might need to restart the application\n");
    if (ButtonCentered("Exit")) {
        ImGui::PopFont();
        ImGui::PopStyleColor();
        return ChangeTo::Quit;
    }
    #if MODE >= 5
    if (ButtonCentered("back to selection")) {
        ImGui::PopFont();
    ImGui::PopStyleColor();
        return ChangeTo::MenuSelection;
    }
    #endif
    ImGui::PopFont();
    ImGui::PopStyleColor();
    return ChangeTo::Error;

}

bool Client::connect() {
    bool success = _connector.connect(sockpp::inet_address(this->server_ip, this->port));
    if (!success) {
        //std::cout << "Failed to connect to server" << std::endl;
        return false;
    }
    _connector.set_non_blocking(true);
    this->server_connected = true;
    this->GimmeGimmenewmsg = true;
    return true;
}

void Client::send_move_to_server(NextMove &move) {
    
    PosIter pos = move.positions;
    u16 board_idx = pos.front().linear_idx();//_from_client_because_they_are_LAPPE_and_used_col_major();
    u16 tile_idx = pos.back().linear_idx();//_from_client_because_they_are_LAPPE_and_used_col_major();
    ClientMessage msg;
    msg.specific = PlaceSymbolProposal{board_idx, tile_idx};
    this->last_sent_msg = msg;
    this->send_msg = true;
    #if MODE >= 2
        std::cout << "Sent message to server & returning" << std::endl;
    #if MODE >= 3
        std::cout << "tile_idx: " << tile_idx << ", board_idx: " << board_idx << std::endl;
        std::cout << "Move sentto server: \n" << "front: " << pos.front().to_string() << std::endl; 
        std::cout << "back: " << pos.back().to_string() << std::endl;
        std::cout << "Linear front: " << board_idx << std::endl;
        std::cout << "Linear back: " << tile_idx << std::endl;
        if(!_connector.is_connected()){
                            std::cout << "\n not connected\n" << std::endl;
                        }
                        if(!_connector.is_open()){
                            std::cout << "\n not open\n" << std::endl;
                        }
    #endif
    #endif
    return;
}
//handles all msg related stuff
void Client::listen_for_msg(bool *should_run) {
    while(!(this->server_connected) && this->isOpen){ //wait for server to connect
        if(!(this->isOpen)){
        #if MODE >= 2
            std::cout << "listen thread returns" << std::endl;
        #endif
            return;
        }  //window closed
    };
    #if MODE >= 2
            std::cout << "listen thread exited first whileloop" << std::endl;
    #endif
    char buffer[1024];
    unsigned message_length = 0;
    while (*should_run) {
        message_length = 0;
    #if MODE >= 2
            std::cout << "listen thread enters nested whileloop. "<< std::endl;
    #endif
        if(this->send_msg){
                    send_message_to_server(this->_connector, this->last_sent_msg);
                    this->send_msg = false;
                }
        bool new_msg = false;
        while (!new_msg && this->isOpen) {
                if(this->send_msg){
                    send_message_to_server(this->_connector, this->last_sent_msg);
                    #if MODE > 0
                        std::cout << "sent message to server: " << _connector.address() << std::endl;
                        std::string s = json(this->last_sent_msg).dump(); 
                        std::cout << "message: " << s << std::endl;
                    #endif
                    this->send_msg = false;
                }
                
                message_length = _connector.read(&buffer, 1024);
                
                
                new_msg = (buffer[0] == '{' || buffer[0] == '[');
                if(!(this->isOpen)){
                    this->last_sent_msg.specific = OpponentLeft{};
                    send_message_to_server(_connector, this->last_sent_msg);
                    #if MODE >= 2
                        std::cout << "listen thread returns" << std::endl;
                    #endif
                    return;
                }
        }
    #if MODE >= 2
            std::cout << "left nested while." << std::endl;
    #endif
        if(!(this->isOpen)){
            this->last_sent_msg.specific = OpponentLeft{};
            send_message_to_server(_connector, this->last_sent_msg);
            #if MODE >= 2
            std::cout << "listen thread returns" << std::endl;
            #endif
            return;
        }
        std::string s(buffer);
        
        // for debugging
    #if MODE >= 1
        std::cout << "Client recieved the following msg: \n";
        for (int i = 0; i < s.length(); i++) {
            std::cout << buffer[i];
        }
        std::cout << std::endl;
    #endif
        json j = json::parse(s);
        this->last_recieved_msg = j.get<ServerMessage>();
        this->handle_msg();
    #if MODE >= 1
        std::cout << "Client handled the following msg like a pro:\n"<< s << std::endl;
    #endif
        //buffer[0] = '\0';
        for(unsigned i = 0; i < 1024; i++){
            buffer[i] = '\0';
        }
        while(!this->GimmeGimmenewmsg){ //wait until Client has decided what to do
        //std::cout << "played: " << played << std::endl;
            if( std::holds_alternative<PlaceSymbolAccepted>(this->last_recieved_msg.specific) || //nothing to be done here
                std::holds_alternative<PlaceSymbolRejected>(this->last_recieved_msg.specific) || //shouldnt happen, otherwise gamstatemachint of client tells us
                std::holds_alternative<SymbolAssignment>(this->last_recieved_msg.specific)    || //nothing to be done
                (this->game_state_machine.total_games() != 0 && std::holds_alternative<GameStart>(this->last_recieved_msg.specific))     //nothing to be done
                ){     
                    this->GimmeGimmenewmsg = true;

                }
                //std::cout << "i cant handle :\n" << json(this->last_recieved_msg).dump() << std::endl;
            if(!(this->isOpen)){
                this->last_sent_msg.specific = OpponentLeft{};
                send_message_to_server(_connector, this->last_sent_msg);
            #if MODE >= 2
                std::cout << "listen thread returns" << std::endl;
            #endif
                return;
            }  //window closed
        }
    }
    #if MODE >= 2
        std::cout << "listen thread returns" << std::endl;
    #endif
    return;
}

void Client::handle_msg() {
    
    if(std::holds_alternative<SymbolAssignment>(this->last_recieved_msg.specific) ){
        this->my_symbol = get_symbol_assignment(this->last_recieved_msg);
        this->opponent = Player((unsigned)(!this->my_symbol));
        //(this->messages).push(1);
        
        this->GimmeGimmenewmsg = false;
    }
    else if(std::holds_alternative<GameStart>(this->last_recieved_msg.specific) ){
        Player first_player = get_game_start(this->last_recieved_msg);
        #if MODE == 4
            PrePlayedGames::init(this->my_symbol, first_player);
        #endif
        if(this->game_state_machine.total_games() == 0){
            this->game_state_machine = GameStateMachine(first_player);
        }
                this->game_state_machine.start_new_game();
        //(this->messages).push(2);
        this->GimmeGimmenewmsg = false;
        this->games_started++;
    }
    else if(std::holds_alternative<PlaceSymbolAccepted>(this->last_recieved_msg.specific) ){
        PosIter pos = get_place_symbol_accepted(this->last_recieved_msg);
        NextMove mv = NextMove(this->game_state_machine.current_player(), PosIter(pos));
        this->last_move_result = this->game_state_machine.play_move(mv);
        //(this->messages).push(3);
        this->GimmeGimmenewmsg = false;
    }
    else if(std::holds_alternative<PlaceSymbolRejected>(this->last_recieved_msg.specific) ){
        //(this->messages).push(4);
        this->GimmeGimmenewmsg = false;
    }else if(std::holds_alternative<OpponentLeft>(this->last_recieved_msg.specific)){
        this->game_state_machine.resign(this->opponent);
        //(this->messages).push(5);
        this->GimmeGimmenewmsg = false;
    }else if(std::holds_alternative<OpponentResigned>(this->last_recieved_msg.specific)){
     this->game_state_machine.resign(this->opponent);
     //this->messages.push(6);
     std::cout<< "resigned\n";
     this->GimmeGimmenewmsg = false;
      
    }else if(std::holds_alternative<RevancheProposal>(this->last_recieved_msg.specific)){
      this->GimmeGimmenewmsg = false;
      
    }else if(std::holds_alternative<RevancheAccepted>(this->last_recieved_msg.specific)){
      this->GimmeGimmenewmsg = false;
      
    }
    else{
        std::cout << "Error: invalid server message:" << 
                    std::endl;
    }
}
void Client::update_vals(int width, int height){
    ourSizes::update(width, height);
    ourPos::update();
    ourFonts::update();
    this->CrossWin.update();
    this->CircleWin.update();
    this->GameOver.update();
    
}

void Client::begin_of_frame() {
  glfwPollEvents();


  // Start the Dear ImGui frame
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();

  
  ImGui::NewFrame();
  int height, width;
    glfwGetWindowSize(this->window, &width, &height);
  
}

void Client::end_of_frame(GLFWwindow *window, ImVec4& clear_color, bool show_scoreboard = true) {
  #ifdef SELECTING_COLORS
  ImGui::PushStyleColor(ImGuiCol_WindowBg, getCol(ImVec4(0.209f,0.311f,0.345f, 0.784f )));
  color_picker();
  ImGui::PopStyleColor();

  ImGui::PushStyleColor(ImGuiCol_WindowBg, getCol(color_hsv));
  ImGui::Begin("Selected Color", NULL, ImGuiWindowFlags_NoTitleBar);
  ImGui::End();
  ImGui::PopStyleColor();

  ImGui::PushStyleColor(ImGuiCol_WindowBg, getCol(color));
  ImGui::Begin("Selected Color2", NULL, ImGuiWindowFlags_NoTitleBar);
  ImGui::End();
  ImGui::PopStyleColor();
  #endif
    if (show_scoreboard) {
    // this->display_on_fixed_window<int>(ourPos::tr_out, ImVec2(ourSizes::window_width, ourPos::bl_out.y),
    //                                    this->light_dark, getCol(ourColors::clear));
    this->display_on_fixed_window<int>(ourPos::tr_out, ImVec2(ourSizes::window_width, ourPos::bl_out.y),
                                       this->scbrd, getCol(ourColors::clear));
    }

  ImGui::Render();
  int display_w, display_h;
  glfwGetFramebufferSize(window, &display_w, &display_h);
  //std::cout << "display_w: " << display_w << ", display_h: " << display_h << std::endl;
  if(ourSizes::window_height != display_w || ourSizes::window_width != display_w){
        //std::cout << "window resized" << std::endl;
        this->update_vals(display_w, display_h);
        ImGui::GetStyle().FrameRounding = 0.5f * ImGui::GetFrameHeight(); 
    }
  glViewport(0, 0, display_w, display_h);
  glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w,
               clear_color.z * clear_color.w, clear_color.w);
  glClear(GL_COLOR_BUFFER_BIT);
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  glfwSwapBuffers(window);
  if(this->server_connected && !this->isOpen && !std::holds_alternative<OpponentLeft>(this->last_sent_msg.specific)){
    this->send_msg = false;
    this->last_sent_msg.specific = OpponentLeft{};
    send_message_to_server(_connector, this->last_sent_msg);
  }
}

int Client::run() {
    this->isOpen = true;
    
    ChangeToRetGui f0 = [this]() -> ChangeTo {
        return this->menu_selection();
    };
    this->menu_selection_ptr = &f0;
    IntRetGui f1 = [this]() -> int {
        return this->scoreboard();
    };
    this->scbrd = &f1;
    ChangeToRetGui f2 = [this]() -> ChangeTo {
        return this->start_menu();
    };
    this->start_menu_ptr = &f2;
    ChangeToRetGui f3 = [this]() -> ChangeTo {
        return this->host_menu();
    };
    this->host_menu_ptr = &f3;
    ChangeToRetGui f4 = [this]() -> ChangeTo {
        return this->join_menu();
    };
    this->join_menu_ptr = &f4;
    ChangeToRetGui f5 = [this]() -> ChangeTo {
        return this->game_over_menu();
    };
    this->game_over_menu_ptr = &f5;
    ChangeToRetGui f6 = [this]() -> ChangeTo {
        return this->ConnectionScreen();
    };
    this->connection_screen_ptr = &f6;
    ChangeToRetGui f7 = [this]() -> ChangeTo {
        return this->run_main_game();
    };
    this->game_screen_ptr = &f7;
    ChangeToRetGui f8 = [this]() -> ChangeTo {
        return this->wait_for_other();
    };
    this->wait_screen_ptr = &f8;
    IntRetGui f9 = [this]() -> int {
        return this->light_or_darkmode();
    };
    this->light_dark = &f9;
    ChangeToRetGui f10 = [this]() -> ChangeTo {
      return this->run_local_main_game();  
    };
    this->local_game_screen_ptr = &f10;
   

   #if MODE == 5
    ChangeTo todo = ChangeTo::MenuSelection;
    #endif
   #if MODE < 5
   bool listener_should_run = true;
    ChangeTo todo = ChangeTo::StartMenu;
    std::thread listener([this, &listener_should_run](){
        this->listen_for_msg(&listener_should_run);
    });
    this->listen_thread = &listener;
    bool listener_joined = false;

   #endif
    
    // Main loop
    while (!glfwWindowShouldClose(window) && isOpen) {
       
        switch (todo) {
            case ChangeTo::StartMenu:
                while(todo == ChangeTo::StartMenu && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>(
                                ImVec2(0,0), ourPos::br_window, 
                                this->start_menu_ptr);
                    end_of_frame(window, ourColors::clear, false);
                }
                
                break;
            case ChangeTo::MenuSelection:
                while(todo == ChangeTo::MenuSelection && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    
                    todo = display_on_fixed_window<ChangeTo>(ImVec2(0, 0), 
                                    ourPos::br_window,
                                     this->menu_selection_ptr);
                    end_of_frame(window, ourColors::clear, false);
                }
                
                break;
            case ChangeTo::HostMenu:

                while(todo == ChangeTo::HostMenu && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>(
                                ImVec2(0,0), ourPos::br_window, 
                                this->host_menu_ptr);
                    end_of_frame(window, ourColors::clear, false);
                }
                
                break;
            case ChangeTo::WaitScreen:
                while(todo == ChangeTo::WaitScreen && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>(ImVec2(0, 0), 
                                    ourPos::br_window,
                                     this->wait_screen_ptr);
                    end_of_frame(window, ourColors::clear, false);
                }
                
                break;
            case ChangeTo::JoinMenu:
                while(todo == ChangeTo::JoinMenu && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>(ImVec2(0, 0), 
                                    ourPos::br_window,
                                     this->join_menu_ptr);//join_menu();
                    end_of_frame(window, ourColors::clear, false);
                }
                
                break;
            case ChangeTo::Quit:
                this->isOpen = false;
    #if MODE < 5
                    listener_should_run = false;

        #if MODE >= 2 
                    std::cout << "listener thread joining in case swich" << std::endl;
        #endif
                    listener.join();
                    listener_joined = true;
        #if MODE >= 2 && MODE < 5
                    std::cout << "listener thread joined in case swich" << std::endl;
        #endif
    #endif
                break;
            case ChangeTo::RunMainGame:
                while(todo == ChangeTo::RunMainGame && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = this->run_main_game();
                    end_of_frame(window, ourColors::clear);
                }
                
                break;
            case ChangeTo::GameOverMenu:
                while(todo == ChangeTo::GameOverMenu && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>( 
                                    ourPos::tl_out,ourPos::br_out,
                                     this->game_over_menu_ptr);
                    end_of_frame(window, ourColors::clear, false);
                }
                break;
            case ChangeTo::ConnectionScreen:
                while(todo == ChangeTo::ConnectionScreen && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>(ImVec2(0, 0), 
                                    ourPos::br_window,
                                     this->connection_screen_ptr);
                    end_of_frame(window, ourColors::clear);
                }
                break;
            case ChangeTo::Error:
                while(todo == ChangeTo::Error && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = display_on_fixed_window<ChangeTo>(ImVec2(0, 0), 
                                    ourPos::br_window,
                                     this->error_screen_ptr);
                    end_of_frame(window, ourColors::clear);
                }
                break;
            case ChangeTo::RunLocalGame:
                if (this->game_state_machine.state() != GameState::InProgress) {
                    this->game_state_machine.start_new_game();
                }
                while(todo == ChangeTo::RunLocalGame && !glfwWindowShouldClose(window) && isOpen){
                    this->begin_of_frame();
                    todo = run_local_main_game();
                    end_of_frame(window, ourColors::clear);
                }
                break;
        }
    }
    this->isOpen = false;
    #if MODE < 5
    listener_should_run = false;
    
    if(!listener_joined){
    #if MODE >= 2
        std::cout << "listener thread joining after while" << std::endl;
    #endif
        listener.join();
        listener_joined = true;
    #if MODE >= 2 && MODE < 5
        std::cout << "listener thread joined after while" << std::endl;
    #endif
    }
    #endif
    return 0;
}

void Client::create_drawdata_inactive(){
    delete this->board;
    this->board = new ImDrawList(ImGui::GetDrawListSharedData());
    this->board->ChannelsSplit(2);
    for(int yo = 0; yo < 3; yo++){
        for(int xo = 0; xo < 3; xo++){
            const GenericPos out = GenericPos(xo, yo);
            draw_inner_board(out);
        }
    }
    this->board->ChannelsMerge();
}

void Client::draw_tile(const GenericPos &out, const GenericPos &inner, bool on_active_board){
        int col = 3*out.x + inner.x;
        int row = 3*out.y + inner.y;
        int ind_col_maj = 9*row + col;
        std::optional<Player> state = game_state_machine.data_->board->tile(out).tile(inner).state;
        
        ImVec2 tl = ourPos::cell_corners[ind_col_maj].first; //top left
        ImVec2 br = ourPos::cell_corners[ind_col_maj].second;//bottom right

        this->draw_bg->ChannelsSetCurrent(0);

        this->draw_bg->AddRect(tl, br, getCol(ourColors::EdgeTile), 10);
        if(on_active_board ){
            if(this->my_symbol != game_state_machine.data_->currentPlayer){
                this->draw_bg->AddRectFilled(tl, br, getCol(ourColors::ActiveTile), 10);
            }else if(this->my_symbol == Player::Circle){
                this->draw_bg->AddRectFilled(tl, br, getCol(ourColors::ActiveCircleTile), 10);
            }else{
                this->draw_bg->AddRectFilled(tl, br, getCol(ourColors::ActiveCrossTile), 10);
            }
        }
        else{
            this->draw_bg->AddRectFilled(tl, br, getCol(ourColors::PassiveTile), 10);
        }
        int symbol_size = ourSizes::symbol_size;
        ImVec2 center   = ourPos::cell_centers[ind_col_maj];
       
        if(!state.has_value()){
    #if MODE != 4
            if((this->my_symbol == game_state_machine.data_->currentPlayer) && on_active_board  &&
                std::holds_alternative<TileBoardStateFree>(
                    this->game_state_machine.data_->board->tiles[out.linear_idx()].board_state) &&
                mouse_tracker::mouse_x > tl.x && mouse_tracker::mouse_x < br.x && 
                mouse_tracker::mouse_y > tl.y && mouse_tracker::mouse_y < br.y){
                    this->draw_bg->ChannelsSetCurrent(1);
                    this->draw_bg->AddRect(ImVec2(tl.x-10, tl.y-10), ImVec2(br.x+10, br.y + 10), getCol(ourColors::EdgeTile),
                                         10, 0, 4.0);
                    this->draw_bg->AddRectFilled(ImVec2(tl.x-10, tl.y-10), ImVec2(br.x + 10, br.y + 10), getCol(ourColors::ActiveHoveredTile));
                    this->draw_bg->ChannelsSetCurrent(0);
                    if ( mouse_tracker::clicked){
                        //add move
                        auto next_move = NextMove(my_symbol, PosIter({(out), (inner)}));
                        #if MODE < 5
                        #if MODE >= 3
                            std::cout << "sending move: " << next_move << std::endl;
                        #endif
                        
                        if (this->server_connected) {
                            this->send_move_to_server(next_move);
                        } else {
                            this->game_state_machine.play_move(next_move);
                        }
                        

                        #else
                            this->game_state_machine.play_move(next_move);
                        #endif
                        
                    }
            }
        #endif
        }
        else if (state == Player::Circle){
                this->draw_bg->AddRect(tl, br, getCol(ourColors::EdgeTile), 10, 0);

                this->draw_bg->AddCircle(center, symbol_size/2., getCol(ourColors::Circle), 0, ourSizes::symbol_thickness_cell);
        }

        else if(state == Player::Cross){
            this->draw_bg->AddRect(tl, br, getCol(ourColors::EdgeTile), 10);
            this->draw_bg->AddLine(ourPos::cell_cross_bd[ind_col_maj].first, ourPos::cell_cross_bd[ind_col_maj].second,
                                 getCol(ourColors::Cross), ourSizes::symbol_thickness_cell);
            this->draw_bg->AddLine(get_tr_cell(ind_col_maj), get_bl_cell(ind_col_maj), getCol(ourColors::Cross), ourSizes::symbol_thickness_cell);
        }

}

void Client::draw_inner_board(const GenericPos &out){
    int ind = 3*out.y + out.x;
    TileBoardState state = game_state_machine.data_->board->tile(out).state_as_tile();
    bool active = ( !game_state_machine.data_->currentOuterPos.has_value() ||
                    out.x == game_state_machine.data_->currentOuterPos.value().x &&
                    out.y == game_state_machine.data_->currentOuterPos.value().y && 
                    std::holds_alternative<TileBoardStateFree>(state) && 
                    !(this->game_state_machine.state_ == GameState::Finished));
    for(int yi = 0; yi < 3; yi++){
        for(int xi = 0; xi < 3; xi++){
            const GenericPos inner(xi, yi);
            draw_tile(out, inner, active);
        }
    }
    int symbol_size = 2*(ourSizes::innerBoard)/3.;
    if(std::holds_alternative<TileBoardStateWon>(state)){
        Player winner = std::get<TileBoardStateWon>(state).player;
    
        
        
        if(winner == Player::Circle){
            this->draw_bg->AddCircle(ourPos::inner_board_centers[ind], symbol_size/2., getCol(ourColors::Circle), 0, ourSizes::symbol_thickness_board);
            this->draw_bg->AddRect(ourPos::inner_board_corners[ind].first, ourPos::inner_board_corners[ind].second
                                    , getCol(ourColors::Circle), 10, 0, ourSizes::symbol_thickness_cell);
        }
        else if(winner == Player::Cross){
            
            this->draw_bg->AddLine(ourPos::inner_board_cross_bd[ind].first, ourPos::inner_board_cross_bd[ind].second, getCol(ourColors::Cross), ourSizes::symbol_thickness_board);

            this->draw_bg->AddLine(get_tr_inner_board(ind), get_bl_inner_board(ind), getCol(ourColors::Cross), ourSizes::symbol_thickness_board);
            this->draw_bg->AddRect(ourPos::inner_board_corners[ind].first, ourPos::inner_board_corners[ind].second,
                                 getCol(ourColors::Cross), 10, 0, ourSizes::symbol_thickness_cell);

        }
    }

}

void ToggleButton(const char* str_id, bool* v, ImDrawList *draw_list)
{
    ImVec2 p = ImGui::GetCursorScreenPos();

    float height = ImGui::GetFrameHeight();
    float width = height * 2.f;
    float radius = height * 0.50f;

    if (ImGui::InvisibleButton(str_id, ImVec2(width, height)))
        *v = !*v;
    ImU32 col_bg;
    if (ImGui::IsItemHovered())
        col_bg = IM_COL32(218-20, 218-20, 218-20, 255);
    else
        col_bg = IM_COL32(218, 218, 218, 255);

    draw_list->AddRectFilled(p, ImVec2(p.x + width, p.y + height), col_bg, height * 0.5f);
    ImGui::SameLine();
    // not working yet because our font doesn't include unicode characters
    // if (*v) {
    //     ImGui::SetCursorPosX(p.x + width * .2);
    //     ImGui::Text("");
    //     ImGui::SameLine();
    // } else {
    //     ImGui::SetCursorPosX(p.x + width * .6);
    //     ImGui::Text("☼");
    //     ImGui::SameLine();
    // }
    draw_list->AddCircleFilled(ImVec2(*v ? (p.x + width - radius) : (p.x + radius), p.y + radius), radius - 1.5f, IM_COL32(255, 255, 255, 255));
}

int Client::light_or_darkmode() {
    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");

    bool new_dark_mode = this->dark_mode;

    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    
    ImGui::PushFont(ourFonts::thin);
    ImGui::SameLine();
    ToggleButton("toggle", &new_dark_mode, this->draw_window);
    if (new_dark_mode != this->dark_mode) {
        //only change colors if it has changed
        this->dark_mode = new_dark_mode;
        if (this->dark_mode) {
            ourColors::darkMode();
        } else {
            ourColors::lightMode();
        }
    } 
    auto windowWidth = ImGui::GetWindowSize().x;

    float exit_size =  1.2f * ImGui::GetFrameHeight();


    ImGui::SetCursorPosX(windowWidth - exit_size);
    if (ImGui::Button(" X ")) {
        this->isOpen = false;
    }
    
    
    ImGui::PopStyleColor();
    ImGui::PopFont();
    
    
        // ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    // if(ButtonCentered("Dark mode", ImVec2(0, ourSizes::button_height))){
    // } 
    // ImGui::SameLine();
    // if(ButtonCentered("Light mode", ImVec2(0, ourSizes::button_height))){
    //     ourColors::lightMode();
    // }
    // ImGui::PopStyleColor();
    return 0;
}

int Client::scoreboard(){

    IM_ASSERT(ImGui::GetCurrentContext() != NULL && "Missing Dear ImGui context. Refer to examples app!");
    light_or_darkmode();
    
    ImGui::PushStyleColor(ImGuiCol_Text, getCol(ourColors::text));
    ImGui::PushFont(ourFonts::bold);
    TextCentered("Scoreboard:");
    ImGui::PopFont();
    ImGui::PushFont(ourFonts::thin);
    TextCentered("X has " + std::to_string(this->game_state_machine.stats().cross_wins) + " wins");
    TextCentered("O has " + std::to_string(this->game_state_machine.stats().circle_wins) + " wins");
    TextCentered("There were " + std::to_string(this->game_state_machine.stats().draws) + " draws");

    ImGui::PopFont();

    ImGui::SetCursorPosY((ourPos::br_out.y - ourPos::tr_out.y)/3.);
    ImGui::PushFont(ourFonts::bold);
    TextCentered("current player: ");
    ImVec2 cur_pos = ImGui::GetCursorScreenPos();

    ImVec2 center = cur_pos + ImVec2(ImGui::GetWindowSize().x/2, ourSizes::symbol_size/2. + ourSizes::spaceBig);
    if(game_state_machine.data_->currentPlayer == Player::Circle){
        this->draw_window->AddCircle(center, ourSizes::symbol_size/2., getCol(ourColors::Circle), 
                                        0, ourSizes::symbol_thickness_cell);
    }
    else{
        draw_cross(center, ourSizes::symbol_size, this->draw_window, ourSizes::symbol_thickness_cell);
    }
    ImGui::SetCursorPosY((ourPos::br_out.y - ourPos::tr_out.y)/3. + ourSizes::symbol_size + ourSizes::boldFont*ourFonts::bold->Scale + 2*ourSizes::spaceBig);
    TextCentered("your symbol: ");
    cur_pos = ImGui::GetCursorScreenPos();
    center = cur_pos + ImVec2(ImGui::GetWindowSize().x/2, ourSizes::symbol_size/2. + ourSizes::spaceBig);
    if(this->my_symbol == Player::Circle){
        this->draw_window->AddCircle(center, ourSizes::symbol_size/2., getCol(ourColors::Circle),
                                         0, ourSizes::symbol_thickness_cell);
    }
    else{
        draw_cross(center, ourSizes::symbol_size, this->draw_window, ourSizes::symbol_thickness_cell);
    }

    ImGui::SetCursorPosY((ourPos::br_out.y - ourPos::tr_out.y)/3. + 2*ourSizes::symbol_size + ourSizes::boldFont*ourFonts::bold->Scale*2 + 8*ourSizes::spaceBig);

    if(this->game_state_machine.current_player() == this->my_symbol){
        if(ButtonCentered("Resign")){
            this->game_state_machine.resign(this->my_symbol);
            this->last_sent_msg.specific = OpponentResigned{};
            this->send_msg = true;
        }
    }
    // if(ButtonCentered("Exit")){
    //     this->isOpen = false;
    //     ImGui::PopFont();
    //     ImGui::PopStyleColor();
    //     return -1;
    // }

    ImGui::PopFont();
    ImGui::PopStyleColor();
    return 0;
}

Client::Client():   server_running_locally(false), server_connected(false), send_msg(false), tried_to_connect(false),
                    port(42069), server_ip("localhost"), anim_running(false), GimmeGimmenewmsg(false) {
    // Setup glfw window
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        std::exit(1);
    ourColors::darkMode();
    // Decide GL+GLSL versions
 #if defined(IMGUI_IMPL_OPENGL_ES2)
    // GL ES 2.0 + GLSL 100
  const char *glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
 #elif defined(__APPLE__)
    // GL 3.2 + GLSL 150
  const char *glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);           // Required on Mac
 #else
    // GL 3.0 + GLSL 130
    const char *glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    // glfwWindowHint(GLFW_SAMPLES,32);
    // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+
    // only glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // 3.0+ only
 #endif
    GLFWmonitor *MyMonitor = glfwGetPrimaryMonitor(); // The primary monitor
    const GLFWvidmode *mode = glfwGetVideoMode(MyMonitor);
    int scr_width = mode->width;
    int scr_height = mode->height;
    // Create window with graphics context
    window =
            glfwCreateWindow(scr_width/2, scr_height/2, "My Title", nullptr, nullptr); //always fullscreen
    if (window == nullptr)
        std::exit(1);
    glfwSetMouseButtonCallback(window, &mouse_tracker::MouseButtonCallback);
    glfwSetCursorPosCallback(window, &mouse_tracker::CursorPosCallback);
    glfwSetKeyCallback(window, &mouse_tracker::KeyCallback);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    //ImGuiIO &io = ImGui::GetIO();
    this->io = ImGui::GetIO();
    (void) io;
    io.ConfigFlags |=
            ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
    io.ConfigFlags |=
            ImGuiConfigFlags_NavEnableGamepad; // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    this->game_state_machine = GameStateMachine(Player::Cross);
    
    // ImGui::StyleColorsLight();
    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Setup Dear ImGui style
    // color of tiles
    ImGui::PushStyleColor(ImGuiCol_Header, ourColors::indicator);
    ImGui::PushStyleColor(ImGuiCol_HeaderHovered, ourColors::indicator);
    ImGui::PushStyleColor(ImGuiCol_HeaderActive, ourColors::indicator);
    // titlebar, active = in game
    ImGui::PushStyleColor(ImGuiCol_TitleBg, ourColors::indicator);
    ImGui::PushStyleColor(ImGuiCol_TitleBgActive, ourColors::indicator);
    ImGui::PushStyleColor(ImGuiCol_TitleBgCollapsed, ourColors::indicator);
    // color of text on the top
    ImGui::PushStyleColor(ImGuiCol_Text, ourColors::black);
    // color of background (between tiles)
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ourColors::bg);
    //setup our stuff
    ourSizes::init(scr_width/2., scr_height/2.);
    ourPos::update();
    ourFonts::init(io);
    this->CrossWin = CrossWinAnim();
    this->CircleWin = CircleWinAnim();
    this->GameOver = GameOverAnim();
}

Client::~Client() {
    // Cleanup
    ImGui::PopStyleColor(8);

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
}
