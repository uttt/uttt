#include "imgui_vals.hpp"
#include "imgui.h"


bool mouse_tracker::clicked = false;
int mouse_tracker::mouse_x = 0;
int mouse_tracker::mouse_y = 0;




ImVec4 ourColors::tile = ImVec4(0.95f, 0.8f, 0.35f, 1.f);
ImVec4 ourColors::tile_passive = ImVec4(0.38f, 0.7f, 0.38f, 1.f);
ImVec4 ourColors::tile_pressed = ImVec4(0.4f, 0.6f, 0.4f, 1.f);
ImVec4 ourColors::tile_hovered = ImVec4(0.f, 0.f, 1.f, 1.f);
ImVec4 ourColors::black = ImVec4(0.f, 0.f, 0.f, 1.f);
ImVec4 ourColors::ActiveTile = ImVec4(0.4f, 0.4f, 0.4f, 0.6f);
ImVec4 ourColors::ActiveCrossTile = ImVec4(0.6f, 0.4f, 0.4f, 0.6f);
ImVec4 ourColors::ActiveCircleTile = ImVec4(0.4f, 0.4f, 0.6f, 0.6f);
ImVec4 ourColors::ActiveHoveredTile = ImVec4(0.3f, 0.3f, 0.3f, 1.f);
ImVec4 ourColors::PassiveTile = ImVec4(0.2f, 0.2f, 0.2f, 1.f);
ImVec4 ourColors::indicator = ImVec4(0.f, 1.f, 1.f, 1.f);

ImVec4 ourColors::EdgeTile = ImVec4(0.6f, 0.6f, 0.6f, 1.f);
ImVec4 ourColors::scoreboard = ImVec4(0.f, .5f, 0.f, 0.8f);
ImVec4 ourColors::text = ImVec4(0.628f, 0.758f, 0.822f ,1.f);
ImVec4 ourColors::title_bar_color = ourColors::tile;
ImVec4 ourColors::clear = ImVec4(0.117f, 0.129f, 0.134f, 1.f);
ImVec4 ourColors::bg = ourColors::clear;
ImVec4 ourColors::Cross = ImVec4(0.8f, 0, 0, 1.f);
ImVec4 ourColors::Circle = ImVec4(0, 0, 0.8f, 1.f);
ImVec4 ourColors::white = ImVec4(1.f, 1.f, 1.f, 1.f);

void ourColors::darkMode() {
    ourColors::tile = ImVec4(0.95f, 0.8f, 0.35f, 1.f);
    ourColors::tile_passive = ImVec4(0.38f, 0.7f, 0.38f, 1.f);
    ourColors::tile_pressed = ImVec4(0.4f, 0.6f, 0.4f, 1.f);
    ourColors::tile_hovered = ImVec4(0.f, 0.f, 1.f, 1.f);
    ourColors::black = ImVec4(0.f, 0.f, 0.f, 1.f); 
    ourColors::indicator = ImVec4(0.f, 1.f, 1.f, 1.f);
    ourColors::ActiveTile = ImVec4(0.4f, 0.4f, 0.4f, 0.6f);
    ourColors::ActiveCrossTile = ImVec4(0.6f, 0.4f, 0.4f, 0.6f);
    ourColors::ActiveCircleTile = ImVec4(0.4f, 0.4f, 0.6f, 0.6f);
    ourColors::ActiveHoveredTile = ImVec4(0.3f, 0.3f, 0.3f, 1.f);
    ourColors::PassiveTile = ImVec4(0.2f, 0.2f, 0.2f, 1.f); 
    ourColors::EdgeTile = ImVec4(0.6f, 0.6f, 0.6f, 1.f);
    ourColors::scoreboard = ImVec4(0.f, .5f, 0.f, 0.8f); 
    ourColors::text = ImVec4(0.628f, 0.758f, 0.822f ,1.f);
    ourColors::title_bar_color = ourColors::tile; 
    ourColors::white = ImVec4(1.f, 1.f, 1.f, 1.f);
    ourColors::clear = ImVec4(0.117f, 0.129f, 0.134f, 1.f);
    ourColors::bg = ourColors::clear;
    ourColors::Cross = ImVec4(0.8f, 0, 0, 1.f);
    ourColors::Circle = ImVec4(0, 0, 0.8f, 1.f);
}


void ourColors::lightMode() {
   ourColors::tile = ImVec4(0.95f, 0.95f, 0.95f, 1.f);
   ourColors::tile_passive = ImVec4(0.8f, 0.95f, 0.8f, 1.f);
   ourColors::tile_pressed = ImVec4(0.6f, 0.8f, 0.6f, 1.f);
   ourColors::tile_hovered = ImVec4(1.f, 1.f, 0.f, 1.f);
   ourColors::black = ImVec4(1.f, 1.f, 1.f, 1.f);
   ourColors::indicator = ImVec4(0.f, 1.f, 1.f, 1.f);
   ourColors::white = ImVec4(0.f, 0.f, 0.f, 1.f);

   ourColors::ActiveTile = ImVec4(0.8f, 0.8f, 0.8f, 0.6f);
   ourColors::ActiveCrossTile = ImVec4(1.f, 0.8f, 0.8f, 0.6f);
    ourColors::ActiveCircleTile = ImVec4(0.8f, 0.8f, 1.f, 0.6f);
   ourColors::ActiveHoveredTile = ImVec4(0.7f, 0.7f, 0.7f, 1.f);
   ourColors::PassiveTile = ImVec4(0.7f, 0.7f, 0.7f, 1.f);

   ourColors::EdgeTile = ImVec4(0.6f, 0.6f, 0.6f, 1.f);
   ourColors::scoreboard = ImVec4(1.f, 0.5f, 0.5f, 0.8f);

   ourColors::bg = ImVec4(0.882f, 0.871f, 0.886f, .5f);
   ourColors::text = ImVec4(0.372f, 0.242f, 0.178f, 1.f);  // Adjust text color for better visibility on white background
   ourColors::title_bar_color = ourColors::tile;

   ourColors::clear = ImVec4(0.882f, 0.871f, 0.866f, 1.f);
   ourColors::Cross = ImVec4(0.8f, 0, 0, 1.f);
   ourColors::Circle = ImVec4(0, 0, 0.8f, 1.f);
    
}

ImVec2 ourPos::cn_out = ImVec2(0,0); 
ImVec2 ourPos::tl_out = ImVec2(0,0); 
ImVec2 ourPos::tr_out = ImVec2(0,0); 
ImVec2 ourPos::bl_out = ImVec2(0,0);
ImVec2 ourPos::br_out = ImVec2(0,0);

ImVec2 ourPos::br_window = ImVec2(0,0);

std::array<std::pair<ImVec2, ImVec2>, 81> ourPos::cell_corners = std::array<std::pair<ImVec2, ImVec2>, 81>();
std::array<std::pair<ImVec2, ImVec2>, 81> ourPos::cell_cross_bd = std::array<std::pair<ImVec2, ImVec2>, 81>();
std::array<ImVec2, 81> ourPos::cell_centers = std::array<ImVec2, 81>();
std::array<std::pair<ImVec2, ImVec2>, 9> ourPos::inner_board_corners = std::array<std::pair<ImVec2, ImVec2>, 9>();
std::array<std::pair<ImVec2, ImVec2>, 9> ourPos::inner_board_cross_bd = std::array<std::pair<ImVec2, ImVec2>, 9>();
std::array<ImVec2, 9> ourPos::inner_board_centers = std::array<ImVec2, 9>();

void ourPos::update(){
    ourPos::cn_out = ImVec2( ourSizes::window_width/2., ourSizes::window_height/2.);
    ourPos::tl_out = ImVec2( ourSizes::window_width/2. - ourSizes::outerBoard/2., ourSizes::window_height/2. - ourSizes::outerBoard/2.);
    ourPos::tr_out = ImVec2( ourSizes::window_width/2. + ourSizes::outerBoard/2., ourSizes::window_height/2. - ourSizes::outerBoard/2.);
    ourPos::bl_out = ImVec2( ourSizes::window_width/2. - ourSizes::outerBoard/2., ourSizes::window_height/2. + ourSizes::outerBoard/2.);
    ourPos::br_out = ImVec2( ourSizes::window_width/2. + ourSizes::outerBoard/2., ourSizes::window_height/2. + ourSizes::outerBoard/2.);
    ourPos::br_window = ImVec2( ourSizes::window_width, ourSizes::window_height);
    int big_symbol_size = 2*ourSizes::innerBoard/3;
    for(unsigned row = 0; row < 9; row++){
        int d_row = (row/3)*(ourSizes::spaceBig) + (row - row/3)*(ourSizes::spaceSmall);
            for(unsigned col = 0; col < 9; col++){
                int d_col = (col/3)*(ourSizes::spaceBig) + (col - col/3)*(ourSizes::spaceSmall);
                ImVec2 tl =ImVec2((ourSizes::cellSize) * col + d_col + ourPos::tl_out.x, (ourSizes::cellSize) * row + d_row + ourPos::tl_out.y);
                ImVec2 br = ImVec2((ourSizes::cellSize) * (col + 1) + d_col + ourPos::tl_out.x, (ourSizes::cellSize) * (row + 1) + d_row + ourPos::tl_out.y);
                ImVec2 center = ImVec2((tl.x + br.x)/2., (tl.y + br.y)/2.);
                ImVec2 tl_cross(center.x - ourSizes::symbol_size/2., center.y - ourSizes::symbol_size/2.);
                ImVec2 br_cross(center.x + ourSizes::symbol_size/2., center.y + ourSizes::symbol_size/2.);

                ourPos::cell_centers[9*row + col] = center;
                ourPos::cell_corners[9*row + col] = std::make_pair(tl, br);
                ourPos::cell_cross_bd[9*row + col] = std::make_pair(tl_cross, br_cross);
            }
            
        }
        for (unsigned row = 0; row < 9; row ++){
            unsigned row_out = row/3;
            unsigned col_out = row%3;

            ImVec2 center_board   = ourPos::cell_centers[9*(3*row_out+1) + 3*col_out+1];
            ImVec2 tl_board = ImVec2(center_board.x - ourSizes::innerBoard/2., center_board.y - ourSizes::innerBoard/2.);
            ImVec2 br_board = ImVec2(center_board.x + ourSizes::innerBoard/2., center_board.y + ourSizes::innerBoard/2.);
            ImVec2 tl_board_cross = ImVec2(center_board.x - big_symbol_size/2., center_board.y - big_symbol_size/2.);
            ImVec2 br_board_cross = ImVec2(center_board.x + big_symbol_size/2., center_board.y + big_symbol_size/2.);
            ourPos::inner_board_centers[row] = center_board;
            ourPos::inner_board_corners[row] = std::make_pair(tl_board, br_board);
            ourPos::inner_board_cross_bd[row] = std::make_pair(tl_board_cross, br_board_cross);
        } 

        
}

int ourSizes::screen_width = 0;
int ourSizes::screen_height = 0;
int ourSizes::window_width = 0;
int ourSizes::window_height = 0;
int ourSizes::old_window_width = 0;
int ourSizes::old_window_height = 0;
int ourSizes::symbolFont = 0;
int ourSizes::boldFont = 0;
int ourSizes::thinFont = 0;
int ourSizes::hugeFont = 0;
int ourSizes::gameOverFont = 0;
int ourSizes::cellSize = 0;
int ourSizes::spaceBig = 0;
int ourSizes::spaceSmall = 0;
int ourSizes::innerBoard = 0;
int ourSizes::outerBoard = 0;
int ourSizes::symbol_size = 0;
int ourSizes::button_height = 0;
int ourSizes::symbol_thickness_cell = 0;
int ourSizes::symbol_thickness_board = 0;
ImVec2 ourSizes::DummySmall = ImVec2(0,0);
ImVec2 ourSizes::DummyBig = ImVec2(0,0);
ImVec2 ourSizes::alignment = ImVec2(0,0);
ImVec2 ourSizes::cell = ImVec2(0,0);

void ourSizes::init(int scr_width , int scr_height){
    double factor = double(scr_width)/1280.0;
    ourSizes::screen_width = scr_width;
    ourSizes::screen_height = scr_height;
    ourSizes::window_width = scr_width;
    ourSizes::window_height = scr_height;
    ourSizes::old_window_width = scr_width;
    ourSizes::old_window_height = scr_height;
    ourSizes::boldFont = 50*factor;
    ourSizes::thinFont = 25*factor;
    ourSizes::hugeFont = 100*factor;
    ourSizes::symbolFont = 70*factor;
    ourSizes::cellSize = 70*factor;
    ourSizes::gameOverFont = 2*ourSizes::cellSize;
    ourSizes::hugeFont = 140*factor;
    ourSizes::spaceBig = 10*factor;
    ourSizes::spaceSmall = 3*factor;
    ourSizes::symbol_size = cellSize/2;
    ourSizes::innerBoard = 3*cellSize + 2*spaceSmall;
    ourSizes::outerBoard = 3*innerBoard + 2*spaceBig;
    ourSizes::DummySmall = ImVec2(factor, factor);
    ourSizes::DummyBig = ImVec2(5.*factor, 5.*factor);
    ourSizes::alignment = ImVec2(0.5, 0.5);
    ourSizes::button_height = 2*cellSize/3;
    ourSizes::cell = ImVec2(70*factor, 70*factor);
    ourSizes::symbol_thickness_cell = 0.1*ourSizes::cellSize;
    ourSizes::symbol_thickness_board = 0.2*ourSizes::cellSize;
    return;
}
void ourSizes::update(int new_window_width , int new_window_height){
    double factor = double(window_width)/1280.0;
    ourSizes::window_width = new_window_width;
    ourSizes::window_height = new_window_height;
    ourSizes::old_window_width = new_window_width;
    ourSizes::old_window_height = new_window_height;
    //ourSizes::boldFont = 50*factor;
    //ourSizes::thinFont = 25*factor;
    //ourSizes::hugeFont = 100*factor;
    ourSizes::symbolFont = 70*factor;
    ourSizes::cellSize = 70*factor;
    //ourSizes::gameOverFont = ourSizes::cellSize;
    //ourSizes::hugeFont = 140*factor;
    ourSizes::spaceBig = 10*factor;
    ourSizes::spaceSmall = 3*factor;
    ourSizes::symbol_size = cellSize/2;
    ourSizes::innerBoard = 3*cellSize + 2*spaceSmall;
    ourSizes::outerBoard = 3*innerBoard + 2*spaceBig;
    ourSizes::DummySmall = ImVec2(factor, factor);
    ourSizes::DummyBig = ImVec2(5.*factor, 5.*factor);
    ourSizes::alignment = ImVec2(0.5, 0.5);
    ourSizes::button_height = 2*cellSize/3;
    ourSizes::cell = ImVec2(70*factor, 70*factor);
    ourSizes::symbol_thickness_cell = 0.1*ourSizes::cellSize;
    ourSizes::symbol_thickness_board = 0.2*ourSizes::cellSize;
    return;
}

ImFont *ourFonts::bold = nullptr;
ImFont *ourFonts::thin = nullptr;
ImFont *ourFonts::symbol = nullptr;
ImFont *ourFonts::huge = nullptr;
ImFont *ourFonts::GameOver = nullptr;

void ourFonts::init(ImGuiIO &io){
    ourFonts::thin =
        io.Fonts->AddFontFromFileTTF("deps/imgui/misc/fonts/DroidSans.ttf", ourSizes::thinFont);
    ourFonts::bold = 
        io.Fonts->AddFontFromFileTTF("deps/imgui/misc/fonts/Roboto-Medium.ttf", ourSizes::boldFont);
    ourFonts::symbol = 
        io.Fonts->AddFontFromFileTTF("deps/imgui/misc/fonts/Roboto-Medium.ttf", ourSizes::symbolFont);
    ourFonts::huge = 
        io.Fonts->AddFontFromFileTTF("deps/imgui/misc/fonts/Roboto-Medium.ttf", ourSizes::hugeFont);
    ourFonts::GameOver = 
        io.Fonts->AddFontFromFileTTF("deps/imgui/misc/fonts/Roboto-Medium.ttf", ourSizes::gameOverFont);
}
void ourFonts::update(){
    float s = 2.*(float)ourSizes::cellSize/(float)ourSizes::gameOverFont;
    ourFonts::bold->Scale = s;
    ourFonts::thin->Scale = s;
    ourFonts::symbol->Scale = s;
    ourFonts::huge->Scale = s;
    ourFonts::GameOver->Scale = s;
}

