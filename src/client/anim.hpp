#ifndef UTTT_ANIM_HPP
#define UTTT_ANIM_HPP

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
//sockpp deps
#include <sockpp/tcp_connector.h>
#include <sockpp/tcp_socket.h>
//common deps
#include "../common/message.hpp"
#include "../common/common.hpp"
#include "../common/game.h"
//server deps
#include "../server/server.hpp"

//std deps

#include <stdio.h>
#include <cstdlib>
#include <thread>
#include <variant>
#include <queue>
#include <chrono>
#include <cmath>
//imgui deps
#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "imgui_vals.hpp"

#include "helpers.hpp"

#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
using CrossData = std::array<ImVec2, 4>;

enum class Anims{
    CrossWin,
    CircleWin,
    Draw
};


class Animations{
    public:
        Animations();
        void start();
        virtual bool goon(ImDrawList* draw_list, bool should_run) = 0;
        virtual void update(); 
        
    private:
    protected:
        using TimePoint = std::chrono::time_point<std::chrono::system_clock>;
        TimePoint anim_start;
        TimePoint last_frame_start;
        bool running;
        double PI = 3.141592653589793238463;

        float dt_ms = 1000./180.;
        static ImVec2 center;
        static float symbol_width_half;
        static int symb_thickness;
        int update_count = 0;
        int count = -1; 


          
};

class CrossWinAnim : public Animations{
    public:
        CrossWinAnim();
        bool goon(ImDrawList* draw_list, bool should_run) final;
        void update() final;
        
    private:
        static constexpr int N_frames = 150;

        std::array<ImVec2, N_frames> points;

        float rad_inc;
        int dist_corners;
        int tr_ind;
        
        float symbol_diag_half;
        float current_symbol_diag_half;
        float diag_per_2count;
        bool rotating;
        void draw_cross(ImDrawList* draw, ImU32 col);
    
};

class CircleWinAnim : public Animations{
    public:
        CircleWinAnim();
        bool goon(ImDrawList* draw_list, bool should_run) final;
        void update() final;
        
        
    private:
        static constexpr int N_circles = 200;
        std::array<ImVec2, N_circles> small_circles;
        std::array<float, N_circles> rad_small_circles;
        
        float rad_inc;
        
        float current_symbol_width_half;
        bool rotating;
        
        void draw_circle(ImDrawList* draw, ImU32 col);
    
};

class GameOverAnim : public Animations{
    public:
        GameOverAnim();
        bool goon(ImDrawList *draw_list, bool should_run);
        void update() final;
        bool started();
       
    private:
        int N_frames = 100;
        float cur_font_scale;
        ImDrawList *draw_window;
        ImVec2 game_tl_end;
        ImVec2 game_sz_end;
        ImVec2 over_tl_end;
        ImVec2 over_sz_end;
        ImVec2 tl_start;
        ImVec2 sz_start;
        ImVec2 game_inc_tl;
        ImVec2 game_inc_sz;
        ImVec2 over_inc_tl;
        ImVec2 over_inc_sz;
        ImVec2 cur_sz;
        ImVec2 continue_tl;
        ImVec2 continue_sz;
};
#endif //UTTT_ANIM_HPP