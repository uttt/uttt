#ifndef _IMGUI_VALS_HPP_
#define _IMGUI_VALS_HPP_
//std deps
#include <stdio.h>
#include<vector>
//imgui deps
#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h> // Will drag system OpenGL headers
//common deps
#include "../common/common.hpp"
#include "../common/message.hpp"



struct ourColors{
  static ImVec4 tile;
  static ImVec4 tile_passive;
  static ImVec4 tile_pressed;
  static ImVec4 tile_hovered;
  static ImVec4 black;
  //to find unused colors
  static ImVec4 indicator;

  // gameboard
  static ImVec4 ActiveTile;
  static ImVec4 ActiveCrossTile;
  static ImVec4 ActiveCircleTile;
  static ImVec4 ActiveHoveredTile;
  static ImVec4 PassiveTile;

  static ImVec4 EdgeTile;
  static ImVec4 scoreboard;

  static ImVec4 bg;
  static ImVec4 text;
  static ImVec4 title_bar_color;

  static ImVec4 clear;
  static ImVec4 Cross;
  static ImVec4 Circle;

  static ImVec4 white;

  static void darkMode();
  static void lightMode();
};

struct ourPos{
    static ImVec2 cn_out; //center of outer board
    static ImVec2 tl_out; //top left of outer board
    static ImVec2 tr_out; //top right of outer board
    static ImVec2 bl_out; //bottom left of outer board
    static ImVec2 br_out; //bottom right of outer board
    
    static ImVec2 br_window; //bottom right of window

    static std::array<std::pair<ImVec2, ImVec2>, 81> cell_corners; //first:top left and second:bottom right of each cell
    static std::array<std::pair<ImVec2, ImVec2>, 81> cell_cross_bd; //first:top left and second:bottom right of each cell
    static std::array<ImVec2, 81> cell_centers; //center of each cell

    static std::array<std::pair<ImVec2, ImVec2>, 9> inner_board_corners;
    static std::array<std::pair<ImVec2, ImVec2>, 9> inner_board_cross_bd;
    static std::array<ImVec2, 9> inner_board_centers; //center of each inner board

    static void update();
};
struct ourSizes{

    static void init(int scr_width = 1280, int scr_height = 720);
    static void update(int window_width = 1280, int window_height = 720);
    
    static int screen_width; //x
    static int screen_height; //y
    static int window_width;
    static int symbol_thickness_cell;
    static int symbol_thickness_board;
    static int window_height;
    static int old_window_width;
    static int old_window_height;
    static int symbolFont;
    static int boldFont;
    static int thinFont;
    static int hugeFont;
    static int gameOverFont;
    static int cellSize;
    static int spaceBig;
    static int spaceSmall;
    static int innerBoard;
    static int outerBoard;
    static int symbol_size;
    static int button_height;

    static ImVec2 DummySmall;
    static ImVec2 DummyBig;
    static ImVec2 alignment;
    static ImVec2 cell;
};
struct ourFonts{
    static void init(ImGuiIO &io);
    static void update();
    static ImFont *bold;
    static ImFont *thin;
    static ImFont *symbol;
    static ImFont *huge;
    static ImFont *GameOver;
};



struct mouse_tracker{
    static bool clicked;
    static int mouse_x;
    static int mouse_y;

    static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
    {
        // ONLY forward mouse data to your underlying app/game.
        ImGuiIO& io = ImGui::GetIO();
        if (!io.WantCaptureMouse)
        {
            if (button == ImGuiMouseButton_Left && action == 1)
            {
                mouse_tracker::clicked = true;
            }
            else if (button == ImGuiMouseButton_Left && action == 0)
            {
                mouse_tracker::clicked = false;
            }
        }
    }

    static void CursorPosCallback(GLFWwindow* window, double xpos, double ypos)
    {
        // ONLY forward mouse data to your underlying app/game.
        ImGuiIO& io = ImGui::GetIO();
        if (!io.WantCaptureMouse)
        {
            mouse_tracker::mouse_x = xpos;
            mouse_tracker::mouse_y = ypos;
        }
    }

    static void KeyCallback(GLFWwindow* window, int key, int scancode, int actions, int mods)
    {
        // For Dear ImGui to work it is necessary to queue if the keyboard signal is already processed by Dear ImGui
        // Only if the keyboard is not already captured it should be used here.
        ImGuiIO& io = ImGui::GetIO();
        if (!io.WantCaptureKeyboard)
        {
        }
    }
};

#endif