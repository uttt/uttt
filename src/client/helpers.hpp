#ifndef _HELPERS_HPP_
#define _HELPERS_HPP_
//includes done in hpp
#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "imgui.h"
#include "imgui_vals.hpp"
#include <system_error>
#include "../common/game.h"

#define CIRCLE "O"
#define CROSS "X"
#define EMPTY " "

//color pallette on window to select colors
#define SELECTING_COLORS
#undef SELECTING_COLORS


static void glfw_error_callback(int error, const char *description) {
  fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}
static void HelpMarker(const char* desc);/*{
    ImGui::TextDisabled("(?)");
    if (ImGui::BeginItemTooltip())
    {
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}*/
extern ImVec4 color;
extern ImVec4 color_hsv;
void color_picker();

std::string to_player_string(std::optional<Player> tile) ;


ImU32 get_color_of_tile(std::optional<Player> state) ;

ImVec2 get_bl_cell(int ind, bool cross = true);
ImVec2 get_tr_cell(int ind, bool cross = true);
ImVec2 get_bl_inner_board(int ind, bool cross = true);
ImVec2 get_tr_inner_board(int ind, bool cross = true);
ImVec2 get_bl(ImVec2 tl,ImVec2 br);
ImVec2 get_tr(ImVec2 tl,ImVec2 br);


static ImU32 getCol(ImVec4 rgba){
        return ImGui::GetColorU32(rgba);
}

void SelectableColor(ImU32 color) ;

void begin_of_frame() ;

void draw_cross(ImVec2 pos, float width, ImDrawList* draw, float thickness = 1.f, int flag = 0);
void draw_cross(ImVec2 tl, ImVec2 br, ImVec2 tr, ImVec2 bl, ImDrawList* draw, float thickness = 1.f);

std::ostream &operator<<(std::ostream &os, const NextMove &move);






#endif //_HELPERS_HPP_