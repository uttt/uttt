#ifndef PROJECT_MESSAGE_HPP
#define PROJECT_MESSAGE_HPP

#include "common.hpp"
#include "pos.hpp"

#include <nlohmann/json.hpp>
#include <variant>
#include <optional>
#include <queue>

using u16 = std::uint16_t;
using json = nlohmann::json;
using OptionalPlayer = std::optional<Player>;

// Client messages
struct PlaceSymbolProposal {
  u16 inner_board_index; // 0-8
  u16 tile_index; // 0-8

  friend void to_json(json &j, const PlaceSymbolProposal &p) {
    j = json{{"inner_board_index", p.inner_board_index},
             {"tile_index", p.tile_index}};
  }

  friend void from_json(const json &j, PlaceSymbolProposal &p) {
    j.at("inner_board_index").get_to(p.inner_board_index);
    j.at("tile_index").get_to(p.tile_index);
  }
};

struct OpponentLeft {
  friend void to_json(json &j, const OpponentLeft &o) {
    j = json {{"opponent_left", nullptr}};
  }

  friend void from_json(const json &j, OpponentLeft &o) {}
};

struct OpponentResigned {
  friend void to_json(json &j, const OpponentResigned &o) {
    j = json {{"opponent_resigned", nullptr}};
  }

  friend void from_json(const json &j, OpponentResigned &o) {}
};

struct RevancheProposal {
  friend void to_json(json &j, const RevancheProposal &s) {
    j = json {{"start_new_game?", nullptr}};
  }

  friend void from_json(const json &j, RevancheProposal &s) {
    
  }
};
struct RevancheAccepted {
  friend void to_json(json &j, const RevancheAccepted &s) {
    j = json {{"start_new_game!", nullptr}};
  }

  friend void from_json(const json &j, RevancheAccepted &s) {}
};

using ClientMessageSpecific =
  std::variant<
    PlaceSymbolProposal,
    OpponentLeft,
    OpponentResigned,
    RevancheAccepted,
    RevancheProposal
  >;

struct ClientMessage {
  ClientMessageSpecific specific;

  friend void to_json(json &j, const ClientMessage &m) {
    std::visit([&j](const auto &v) { j = v; }, m.specific);
  }

  friend void from_json(const json &j, ClientMessage &m) {
    if (j.contains("inner_board_index")) {
      m.specific = j.get<PlaceSymbolProposal>();
    }else if(j.contains("opponent_left")){
      m.specific = j.get<OpponentLeft>();
    }else if(j.contains("opponent_resigned")){
      m.specific = j.get<OpponentResigned>();
    }else if(j.contains("start_new_game?")){
      m.specific = j.get<RevancheProposal>();
    }else if(j.contains("start_new_game!")){
      m.specific = j.get<RevancheAccepted>();
    }else {
      throw std::runtime_error("Invalid client message");
    }
  }
};

// Server messages
struct SymbolAssignment {
  Player symbol;

  friend void to_json(json &j, const SymbolAssignment &a) {
    j = json{{"symbol", a.symbol}};
  }

  friend void from_json(const json &j, SymbolAssignment &a) {
    j.at("symbol").get_to(a.symbol);
  }
};

struct GameStart {
  Player starting_player;

  friend void to_json(json &j, const GameStart &s) {
    j = json{{"starting_player", s.starting_player}};
  }

  friend void from_json(const json &j, GameStart &s) {
    j.at("starting_player").get_to(s.starting_player);
  }
};

struct PlaceSymbolRejected {
  friend void to_json(json &j, const PlaceSymbolRejected &r) {
    j = json {{"invalid_inner_board_pos", nullptr}};
  }

  friend void from_json(const json &j, PlaceSymbolRejected &r) {}
};

struct PlaceSymbolAccepted {
  u16 inner_board_index;
  u16 tile_index;

  friend void to_json(json &j, const PlaceSymbolAccepted &a) {
    j = json{{"inner_board_index", a.inner_board_index},
             {"tile_index", a.tile_index}};
  }

  friend void from_json(const json &j, PlaceSymbolAccepted &a) {
    j.at("inner_board_index").get_to(a.inner_board_index);
    j.at("tile_index").get_to(a.tile_index);
  }
};

using ServerMessageSpecific =
  std::variant<
    SymbolAssignment,
    GameStart,
    PlaceSymbolRejected,
    PlaceSymbolAccepted,
    OpponentLeft,
    OpponentResigned,
    RevancheProposal,
    RevancheAccepted
  >;

struct ServerMessage {
  ServerMessageSpecific specific;

  friend void to_json(json &j, const ServerMessage &m) {
    std::visit([&j](const auto &v) { j = v; }, m.specific);
  }

  friend void from_json(const json &j, ServerMessage &m) {
    if (j.contains("symbol")) {
      m.specific = j.get<SymbolAssignment>();
    }
    else if (j.contains("starting_player")) {
      m.specific = j.get<GameStart>();
    }else if(j.contains("opponent_left")){
      m.specific = j.get<OpponentLeft>();
    }else if(j.contains("opponent_resigned")){
      m.specific = j.get<OpponentResigned>();
    }else if(j.contains("start_new_game?")){
      m.specific = j.get<RevancheProposal>();
    }else if(j.contains("start_new_game!")){
      m.specific = j.get<RevancheAccepted>();
    }
    else if (j.contains("invalid_inner_board_pos")) {
      m.specific = j.get<PlaceSymbolRejected>();
    }
    else {
      if (j.contains("inner_board_index")) {
        m.specific = j.get<PlaceSymbolAccepted>();
      }
      else {
        throw std::runtime_error("Invalid server message");
      }
    }
  }
};

// get data from client message
PosIter get_place_symbol_proposal(const ClientMessage &message);

// get data from server message
Player get_symbol_assignment(const ServerMessage &message);

Player get_game_start(const ServerMessage &message);

PosIter get_place_symbol_accepted(const ServerMessage &message);

// send and receive messages for server
void send_message_to_client(sockpp::tcp_socket &socket,
                            const ServerMessage &message);

void receive_message_from_client(sockpp::tcp_socket &socket,
                                 ClientMessage &message, char *buffer, unsigned buffer_size = 1024);

bool receive_message_from_client_non_blocking(sockpp::tcp_socket &socket,
                                 ClientMessage &message, char *buffer, unsigned buffer_size = 1024);
// send and receive messages for client
void send_message_to_server(sockpp::tcp_connector &connector,
                            const ClientMessage &message);

void receive_message_from_server(sockpp::tcp_connector &connector,
                                 ServerMessage &message);



#endif // PROJECT_MESSAGE_HPP

