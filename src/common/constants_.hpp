#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include "cstddef"
// This holds all the constants used in commons

const size_t LOCAL_BOARD_SIZE = 3;
const size_t N_LINES = 2 * LOCAL_BOARD_SIZE + 2;

#endif  //CONSTANTS_HPP

