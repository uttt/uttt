#ifndef UTTT_POS_HPP
#define UTTT_POS_HPP

#include <cassert>
#include <queue>

#include "common.hpp"
#include "constants_.hpp"

using u16 = std::uint16_t;

struct GenericPos {
    u16 x;
    u16 y;

    GenericPos(u16 x, u16 y) {
        if (x >= LOCAL_BOARD_SIZE || y >= LOCAL_BOARD_SIZE) { throw std::runtime_error("Invalid position"); }
        this->x = x;
        this->y = y;
    }

    [[nodiscard]] u16 linear_idx() const { return this->x + LOCAL_BOARD_SIZE * this->y; }
    [[nodiscard]] u16 linear_idx_from_client_because_they_are_LAPPE_and_used_col_major() const { return this->y + LOCAL_BOARD_SIZE * this->x; }

    [[nodiscard]] static GenericPos fromLinearIdx(u16 idx) {
        assert(idx < LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE);
        return GenericPos(idx % LOCAL_BOARD_SIZE, idx / LOCAL_BOARD_SIZE);
    }

    bool operator==(const GenericPos &other) const {
        return this->x == other.x && this->y == other.y;
    }

    void print() const { std::cout << to_string() << std::endl; }

    [[nodiscard]] std::string to_string() const {
        return "(" + std::to_string(this->x) + ", " + std::to_string(this->y) + ")";
    }
};

using PosIter = std::queue<GenericPos>;


#endif // UTTT_POS_HPP
