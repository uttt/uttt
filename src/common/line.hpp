#ifndef UTTT_LINE_HPP
#define UTTT_LINE_HPP

#include "common.hpp"
#include "pos.hpp"
#include <array>

const u16 LINE_LENGTH = 3;

struct LineStateFree {
};
struct LineStatePartiallyWon {
    Player player;
    u16 count;
};
struct LineStateWon {
    Player player;
};
struct LineStateDrawn {
};
using LineState =
        std::variant<
                LineStateFree,
                LineStatePartiallyWon,
                LineStateWon,
                LineStateDrawn
        >;

bool is_drawn(LineState l);
LineState combine_line_states(LineState a, LineState b);
LineState line_state_from_tile_board_state(TileBoardState t);

struct LineXAxis {
    u16 y;
};
struct LineYAxis {
    u16 x;
};
struct LineMainDiagonal {
};
struct LineAntiDiagonal {
};
using Line =
        std::variant<
                LineXAxis,
                LineYAxis,
                LineMainDiagonal,
                LineAntiDiagonal
        >;

u16 line_idx(Line line);
std::vector<Line> all_lines();
std::vector<Line> all_lines_through_point(GenericPos pos);
std::vector<GenericPos> all_pos_on_line(Line line);

#endif // UTTT_LINE_HPP
