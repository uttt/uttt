#include "line.hpp"

bool is_drawn(LineState l) {
  return std::holds_alternative<LineStateDrawn>(l);
}

LineState combine_line_states(LineState a, LineState b) {
  if (std::holds_alternative<LineStatePartiallyWon>(a) &&
      std::holds_alternative<LineStatePartiallyWon>(b)) {
    auto ai = std::get<LineStatePartiallyWon>(a);
    auto bi = std::get<LineStatePartiallyWon>(b);
    if (ai.player == bi.player) {
      u16 n = ai.count + bi.count;
      assert(n <= LINE_LENGTH);
      if (n == LINE_LENGTH) {
        return LineStateWon{ai.player};
      } else {
        return LineStatePartiallyWon{ai.player, n};
      }
    } else { return LineStateDrawn{}; }
  } else if (std::holds_alternative<LineStatePartiallyWon>(a) && std::holds_alternative<LineStateFree>(b)) {
    return a;
  } else if (
    std::holds_alternative<LineStateFree>(a) && std::holds_alternative<LineStatePartiallyWon>(b)) {
    return b;
  } else if (std::holds_alternative<LineStateFree>(a) &&
             std::holds_alternative<LineStateFree>(b)) {
    return LineStateFree{};
  } else if (std::holds_alternative<LineStateWon>(a) || std::holds_alternative<LineStateWon>(b)) {
    throw std::runtime_error("a winning line should never be combined");
  } else { return LineStateDrawn{}; }
}

LineState line_state_from_tile_board_state(TileBoardState t) {
  if (std::holds_alternative<TileBoardStateFree>(t)) {
    return LineStateFree{};
  } else if (std::holds_alternative<TileBoardStateWon>(t)) {
    auto w = std::get<TileBoardStateWon>(t);
    return LineStatePartiallyWon{w.player, 1};
  } else {
    return LineStateDrawn{};
  }
}

u16 line_idx(Line line) {
  return std::visit(
    [&](auto &&l) -> u16 {
      using T = std::decay_t<decltype(l)>;
      if constexpr (std::is_same_v<T, LineXAxis>) {
        assert(l.y < LINE_LENGTH);
        return l.y;
      } else if constexpr (std::is_same_v<T, LineYAxis>) {
        assert(l.x < LINE_LENGTH);
        return l.x + LINE_LENGTH;
      } else if constexpr (std::is_same_v<T, LineMainDiagonal>) {
        return 2 * LINE_LENGTH;
      } else if constexpr (std::is_same_v<T, LineAntiDiagonal>) {
        return 2 * LINE_LENGTH + 1;
      }
    },
    line);
}

std::vector<Line> all_lines() {
  std::vector<Line> l;
  for (u16 i = 0; i < LINE_LENGTH; ++i) {
    l.emplace_back(LineXAxis{i});
  }
  for (u16 i = 0; i < LINE_LENGTH; ++i) {
    l.emplace_back(LineYAxis{i});
  }
  l.emplace_back(LineMainDiagonal{});
  l.emplace_back(LineAntiDiagonal{});
  return l;
}

std::vector<Line> all_lines_through_point(GenericPos pos) {
  std::vector<Line> l = {LineXAxis{pos.y}, LineYAxis{pos.x}};
  if (pos.x == pos.y) {
    l.emplace_back(LineMainDiagonal{});
  }
  if (pos.x + pos.y == 2) {
    l.emplace_back(LineAntiDiagonal{});
  }
  return l;
}

std::vector<GenericPos> all_pos_on_line(Line line) {
  std::vector<GenericPos> pos;
  std::visit(
    [&](auto &&l) {
      using T = std::decay_t<decltype(l)>;
      if constexpr (std::is_same_v<T, LineXAxis>) {
        for (u16 i = 0; i < 3; ++i) {
          pos.emplace_back(GenericPos{i, l.y});
        }
      } else if constexpr (std::is_same_v<T, LineYAxis>) {
        for (u16 i = 0; i < 3; ++i) {
          pos.emplace_back(l.x, i);
        }
      } else if constexpr (std::is_same_v<T, LineMainDiagonal>) {
        for (u16 i = 0; i < 3; ++i) {
          pos.emplace_back(i, i);
        }
      } else if constexpr (std::is_same_v<T, LineAntiDiagonal>) {
        for (u16 i = 0; i < 3; ++i) {
          pos.emplace_back(i, static_cast<u16>(2 - i));
        }
      }
    },
    line);
  return pos;
}