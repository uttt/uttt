#include "message.hpp"

#undef MODE
#define MODE 0
// get data from client message
PosIter get_place_symbol_proposal(const ClientMessage &message) {
  if (const auto *pos = std::get_if<PlaceSymbolProposal>(&message.specific)) {
    PosIter pos_queue({GenericPos::fromLinearIdx(pos->inner_board_index),
                       GenericPos::fromLinearIdx(pos->tile_index)});
    /*pos_queue.push(GenericPos::fromLinearIdx(pos->tile_index));
    pos_queue.push(GenericPos::fromLinearIdx(pos->inner_board_index));*/
    return pos_queue;
  } else {
    #if MODE > 0
      std::cout << "invalid message in get symbol proposal:" << json(message).dump()  << std::endl;
    #endif
    throw std::runtime_error("Invalid client message in get_symb_prop");
  }
}

// get data from server message
Player get_symbol_assignment(const ServerMessage &message) {
  if (const auto *player = std::get_if<SymbolAssignment>(&message.specific)) {
    return player->symbol;
  } else {
    throw std::runtime_error("Invalid server message");
  }
}
Player get_game_start(const ServerMessage &message) {
  if (const auto *player = std::get_if<GameStart>(&message.specific)) {
    return player->starting_player;
  } else {
    throw std::runtime_error("Invalid server message");
  }
}
PosIter get_place_symbol_accepted(const ServerMessage &message) {
  if (const auto *pos = std::get_if<PlaceSymbolAccepted>(&message.specific)) {
    PosIter pos_queue({GenericPos::fromLinearIdx(pos->inner_board_index),
                       GenericPos::fromLinearIdx(pos->tile_index)});
    /*pos_queue.push(GenericPos::fromLinearIdx(pos->tile_index));
    pos_queue.push(GenericPos::fromLinearIdx(pos->inner_board_index));*/
    return pos_queue;
  } else {
    throw std::runtime_error("Invalid server message");
  }
}

void send_message_to_client(sockpp::tcp_socket &socket,
                            const ServerMessage &message) {
  json j = message;
  std::string s = j.dump();
  const char *message_to_send = s.c_str();
  socket.write_n(message_to_send, s.length());
  #if MODE > 0
    std::cout << "Sent message to " << socket.peer_address() << "\n";
  #endif
}
void send_message_to_server(sockpp::tcp_connector &connector,
                            const ClientMessage &message) {
  json j = message;
  std::string s = j.dump();
  const char *message_to_send = s.c_str();
  connector.write_n(message_to_send, s.length());
  #if MODE > 0
    std::cout << "Sent message to " << connector.peer_address() << "\n";
  #endif
}

void receive_message_from_client(sockpp::tcp_socket &socket,
                                 ClientMessage &message, char *buffer, unsigned buffer_size) {
  for(int i = 0; i < buffer_size; i++){
    buffer[i] = '\0';
  }
  socket.read(buffer, buffer_size);
  auto br = std::find(buffer, buffer + buffer_size, '}');
  if(br == buffer + buffer_size){
      #if MODE > 0
        std::cerr << "no } in message" << std::endl; 
      #endif
      return;    
  }

  std::string s(buffer, br+1);
  #if MODE > 0
    std::cout << "s: " << s << std::endl;
  #endif
  json j = json::parse(s);
  message = j.get<ClientMessage>();
  #if MODE > 0
    std::cout << "Received message from " << socket.peer_address() << "\n";
    std::cout << "message: " << s << std::endl;
  #endif
}
bool receive_message_from_client_non_blocking(sockpp::tcp_socket &socket,
                                 ClientMessage &message, char *buffer, unsigned buffer_size) {
  for(int i = 0; i < buffer_size; i++){
    buffer[i] = '\0';
  }
  socket.read(buffer,buffer_size);
  if(buffer[0] == '[' || buffer[0] == '{'){
    auto br = std::find(buffer, buffer + buffer_size, '}');
    if(br == buffer + buffer_size){
      #if MODE > 0
        std::cerr << "no } in message" << std::endl; 
      #endif
      return false;
    }
    std::string s(buffer, br+1);
    #if MODE > 0
      std::cout << "s: " << s << std::endl;
    #endif
    json j = json::parse(s);
    message = j.get<ClientMessage>();
    #if MODE > 0
      std::cout << "Received message from " << socket.peer_address() << "\n";
      std::cout << "message: " << s << std::endl;
    #endif
    return true;
  }
  return false;
}
void receive_message_from_server(sockpp::tcp_connector &connector, ServerMessage &message) {
  char buffer[30000];
  connector.read_n(buffer, 30000);
  std::string s(buffer);
  json j = json::parse(s);
  message = j.get<ServerMessage>();
  #if MODE > 0
    std::cout << "Received message from " << connector.peer_address() << "\n";
  #endif
}
