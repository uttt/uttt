#ifndef UTTT_GAME_H
#define UTTT_GAME_H

#define VERBOSE 1
//#undef VERBOSE
#include <memory>
#include "common.hpp"
#include "board.hpp"


struct GameData {
    std::unique_ptr<OuterBoard> board;
    Player currentPlayer;
    std::optional<GenericPos> currentOuterPos;

    explicit GameData(
            Player startingPlayer, std::unique_ptr<OuterBoard> &board, std::optional<GenericPos> &currentOuterPos
    )
            : currentPlayer(startingPlayer), currentOuterPos(currentOuterPos), board(std::move(board)) {
    }

    void switchToNextPlayer() { currentPlayer = currentPlayer == Cross ? Circle : Cross; }

};

static std::unique_ptr<GameData> createGameData(Player startingPlayer) {
    auto board = std::make_unique<OuterBoard>();
    std::optional<GenericPos> currentOuterPos = std::nullopt;
    return std::make_unique<GameData>(startingPlayer, board, currentOuterPos);
}

enum GameState {
    NotStarted,
    InProgress,
    Finished
};

struct Scores {
    unsigned long cross_wins = 0;
    unsigned long circle_wins = 0;
    unsigned long draws = 0;
};


struct NextMove {
    Player player;
    PosIter positions;

    NextMove(Player player, PosIter positions) : player(player), positions(std::move(positions)) {}

    NextMove(Player player, PosIter &positions) : player(player), positions(std::move(positions)) {}
};

struct AcceptedWaitForNextMove {
    Player player;
    std::optional<GenericPos> outerPosition;
};

struct GameNotInProgress {
    GameState state;
};

struct WrongPlayer {
    Player expectedPlayer;
};

struct WrongOuterPosition {
    GenericPos expectedPosition;
    GenericPos providedPosition;
};

struct Win {
    Player winner;
};

struct OccupiedPosition {
    // Any additional data fields
};

struct Draw {
    // Any additional data fields
};

struct RequestPlayerToChooseInnerBoard {
    Player player;
};

// Define the MoveResult as a variant of all possible outcomes
using MoveResult = std::variant<
        AcceptedWaitForNextMove,
        GameNotInProgress,
        WrongPlayer,
        WrongOuterPosition,
        Win,
        OccupiedPosition,
        Draw
>;


struct GameStateMachine {

    std::unique_ptr<GameData> data_;
    GameState state_ = GameState::NotStarted;
    Player next_starting_player;
    Scores stats_;

    explicit GameStateMachine(Player startingPlayer = random_player_symbol()) :
            data_(createGameData(startingPlayer)), next_starting_player(startingPlayer), stats_(Scores()) {}
    
   

    bool start_new_game() {
        if (!(state_ == GameState::NotStarted || GameState::Finished == state_)) {
        #ifdef VERBOSE
            std::cout << "Game cannot be restarted, finish it first\n";
        #endif
            return false;
        }
        data_ = createGameData(next_starting_player);
        next_starting_player = Cross == next_starting_player ? Circle : Cross;
        state_ = GameState::InProgress;
        return true;
        }

        MoveResult play_move(NextMove &move) {
        if (state_ != GameState::InProgress) {
    #ifdef VERBOSE
            std::cout << "Game is not in progress, cannot play a move\n";
    #endif
            return GameNotInProgress{state_};
        }
        if (move.player != data_->currentPlayer) {
    #ifdef VERBOSE
            std::cout << "Player tried to make a move, but it is not their turn\n";
    #endif
            return WrongPlayer{data_->currentPlayer};
        }
        auto selectedPosition = move.positions;
        auto outer_pos = selectedPosition.front();
        if (data_->currentOuterPos.has_value() && data_->currentOuterPos.value() != outer_pos) {
    #ifdef VERBOSE
            std::cout << "Player tried to place a symbol on an invalid outer board\n";
            std::cout << "Player chose outer board " << outer_pos.to_string()
                      << " but should have chosen outer board " << data_->currentOuterPos.value().to_string() << "\n";
    #endif
            return WrongOuterPosition{data_->currentOuterPos.value(), outer_pos};
        }
        if (std::holds_alternative<TileBoardStateWon>(data_->board->tile(outer_pos).state_as_tile())) {
    #ifdef VERBOSE
            std::cout << "Player tried to place a symbol on a won outer board\n";
    #endif
            return OccupiedPosition();
        }

        auto success = data_->board->place_symbol_in_tile(selectedPosition, data_->currentPlayer);
        if (!success) {
    #ifdef VERBOSE
            std::cout << "Player tried to place a symbol on an occupied tile\n";
    #endif
            return OccupiedPosition();
        }
        bool game_over = !std::holds_alternative<TileBoardStateFree>(data_->board->state_as_tile());
        if (game_over) {
            if (std::holds_alternative<TileBoardStateWon>(data_->board->state_as_tile())) {
    #ifdef VERBOSE
                std::cout << "Player " << data_->currentPlayer << " won the game\n";
    #endif
                finish_game_(data_->board->board_state);
                return Win{data_->currentPlayer};
            }
            if (std::holds_alternative<TileBoardStateDraw>(data_->board->state_as_tile())) {
    #ifdef VERBOSE
                std::cout << "Game ended in a draw\n";
    #endif
                finish_game_(data_->board->board_state);
                return Draw();
            }
            throw std::runtime_error("Invalid game state in GameStateMachine::play_move");
        }
        data_->switchToNextPlayer();
        assert(!move.positions.empty());
        auto nextOuterPos = move.positions.back();
        if (!std::holds_alternative<TileBoardStateWon>(data_->board->tile(nextOuterPos).state_as_tile())) {
            if (board_is_full(nextOuterPos)) {
                data_->currentOuterPos = std::nullopt;
            } else {
                data_->currentOuterPos = nextOuterPos;
            }
        } else {
            data_->currentOuterPos = std::nullopt;
        }
        return AcceptedWaitForNextMove{data_->currentPlayer, data_->currentOuterPos};
    }

bool resign(Player player) {
        if (state_ != GameState::InProgress) {
    #ifdef VERBOSE
            std::cout << "Game is not in progress, cannot resign\n";
    #endif
            return false;
        }
    #ifdef VERBOSE
        std::cout << "Player " << player << " resigned\n";
    #endif
        finish_game_(TileBoardStateWon{player == Player::Cross ? Player::Circle : Player::Cross});
        return true;
}

    [[nodiscard]] GameState state() const { return state_; }

    [[nodiscard]] Player current_player() const { return data_->currentPlayer; }

    [[nodiscard]] Scores stats() const { return stats_; }

    [[nodiscard]] unsigned long total_games() const { return stats_.cross_wins + stats_.circle_wins + stats_.draws; }


private:
    void finish_game_(const TileBoardState state) {
        state_ = GameState::Finished;
        data_->board->board_state = state;
        auto was_won = std::holds_alternative<TileBoardStateWon>(state);
        if (!was_won) {
            stats_.draws++;
            return;
        }
        auto winner = std::get<TileBoardStateWon>(state).player;
        if (winner == Player::Cross) { stats_.cross_wins++; } else { stats_.circle_wins++; }
    }

    bool board_is_full(GenericPos &outer_pos) const {
        auto &tiles = data_->board->tile(outer_pos).tiles;
        // iterate over the tiles in the outer board
        for (auto &tile: tiles) {
            // if the tile is not won, then the board is not full
            if (!std::holds_alternative<TileBoardStateWon>(tile.state_as_tile())) { return false; }
        }
        // replace the loop with std::ranges::all_of
        std::ranges::all_of(tiles, [](auto &tile) {
            return std::holds_alternative<TileBoardStateWon>(tile.state_as_tile());
        });
        return true;
    }
};


static Player selectStartingPlayer(const TileBoardState state, const Player starting_player) {
    if (std::holds_alternative<TileBoardStateWon>(state)) { return std::get<TileBoardStateWon>(state).player; }
    return starting_player == Player::Cross ? Player::Circle : Player::Cross;
}


#endif //UTTT_GAME_H
