#include "common.hpp"

std::random_device rd;
std::mt19937 gen37(rd());

Player random_player_symbol() {
  std::uniform_int_distribution<> distr(0, 1);
  int r_var = distr(gen37);

  return Player(r_var);
}
