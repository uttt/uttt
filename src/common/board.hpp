#ifndef UTTT_BOARD_HPP
#define UTTT_BOARD_HPP

#include <concepts>
#include <optional>
#include <queue>

#include "line.hpp"
#include "pos.hpp"

struct TrivialTile {
    std::optional<Player> state;

    [[nodiscard]] TileBoardState state_as_tile() const {
        if (this->state.has_value()) {
            if (this->state.value() == Circle) {
                return TileBoardStateWon{Circle};
            } else {
                return TileBoardStateWon{Cross};
            }
        } else {
            return TileBoardStateFree{};
        }
    }

    [[nodiscard]] TrivialTile trivial_tile_in_tile(PosIter &pos_queue) const {
        assert(pos_queue.empty());
        return *this;
    }

    bool place_symbol_in_tile(PosIter &pos_queue, Player symbol) {
        assert(pos_queue.empty());
        if (this->state.has_value()) { return false; }
        this->state = std::optional<Player>(symbol);
        return true;
    }
};

template<typename TileType>
concept TileTrait = requires(TileType tile, PosIter pos_queue,
                             Player symbol) {
    { tile.state_as_tile() } -> std::convertible_to<TileBoardState>;
    { tile.trivial_tile_in_tile(pos_queue) } -> std::convertible_to<TrivialTile>;
    { tile.place_symbol_in_tile(pos_queue, symbol) } -> std::convertible_to<bool>;
};

template<typename BoardType>
concept BoardTrait = requires(BoardType board, GenericPos pos, Line line,
                              PosIter pos_queue, Player symbol) {
    typename BoardType::TileType;
    { board.tile(pos) } -> std::convertible_to<typename BoardType::TileType &>;
    { board.state_as_board() } -> std::convertible_to<TileBoardState>;
    { board.line(line) } -> std::convertible_to<LineState &>;

    { board.trivial_tile_in_board(pos_queue) } -> std::convertible_to<TrivialTile>;
    { board.place_symbol_in_board(pos_queue, symbol) } -> std::convertible_to<void>;
    { board.update_super_state(pos) } -> std::convertible_to<void>;
};

template<typename Tile> requires TileTrait<Tile>
struct GenericBoard {
    using TileType = Tile;

    std::array<Tile, LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE> tiles;
    TileBoardState board_state;
    std::array<LineState, N_LINES> line_states;

    //TileTrait
    [[nodiscard]] TileBoardState state_as_tile() const;

    [[nodiscard]] TrivialTile trivial_tile_in_tile(PosIter &pos_queue);

    bool place_symbol_in_tile(PosIter pos_queue, Player symbol);

    //BoardTrait
    TileType &tile(GenericPos pos);

    [[nodiscard]] TileBoardState state_as_board() const;

    [[nodiscard]] LineState &line(Line line);

    [[nodiscard]] TrivialTile trivial_tile_in_board(PosIter &pos_queue);

    bool place_symbol_in_board(PosIter pos_queue, Player symbol);

    void update_super_state(GenericPos pos);

};

template<typename Tile>
requires TileTrait<Tile>
TileBoardState GenericBoard<Tile>::state_as_tile() const {
    return this->board_state;
}

template<typename Tile>
requires TileTrait<Tile>
TrivialTile GenericBoard<Tile>::trivial_tile_in_tile(PosIter &pos_queue) {
    if (pos_queue.empty()) {
        throw std::runtime_error("Out of valid positions");
    }
    auto pos = pos_queue.front();
    pos_queue.pop();
    return this->tile(pos).trivial_tile_in_tile(pos_queue);
}

template<typename Tile>
requires TileTrait<Tile>
bool GenericBoard<Tile>::place_symbol_in_tile(PosIter pos_queue, Player symbol) {
    if (pos_queue.empty()) {
        throw std::runtime_error("Out of valid positions");
    }
    return this->place_symbol_in_board(pos_queue, symbol);
}

//BoardTrait
template<typename Tile>
requires TileTrait<Tile>
Tile &GenericBoard<Tile>::tile(const GenericPos pos) {
    return this->tiles[pos.linear_idx()];
}

template<typename Tile>
requires TileTrait<Tile>
TileBoardState GenericBoard<Tile>::state_as_board() const {
    return this->board_state;
}

template<typename Tile>
requires TileTrait<Tile>
LineState &GenericBoard<Tile>::line(Line line) {
    return this->line_states[line_idx(line)];
}

template<typename Tile>
requires TileTrait<Tile>
TrivialTile GenericBoard<Tile>::trivial_tile_in_board(PosIter &pos_queue) {
    if (pos_queue.empty()) { throw std::runtime_error("Out of valid positions"); }
    auto pos = pos_queue.front();
    pos_queue.pop();
    return this->tile(pos).trivial_tile_in_tile(pos_queue);
}

template<typename Tile>
requires TileTrait<Tile>
bool GenericBoard<Tile>::place_symbol_in_board(PosIter pos_queue, Player symbol) {
    if (pos_queue.empty()) {
        throw std::runtime_error("Out of valid positions");
    }
    auto pos = pos_queue.front();
    pos_queue.pop();
    bool success = this->tile(pos).place_symbol_in_tile(pos_queue, symbol);
    if (success) { this->update_super_state(pos); }
    return success;
}

/*template<typename Tile>
requires TileTrait<Tile>
void GenericBoard<Tile>::update_super_state(GenericPos pos) {
    for (auto line: all_lines_through_point(pos)) {
        bool seen_cross = false, seen_circle = false, seen_free = false;
        auto line_state = LineState{LineStateFree{}};
        for (auto &position: all_pos_on_line(line)) {
            auto tile = this->tile(position);
            auto tile_state = tile.state_as_tile();
            if (std::holds_alternative<TileBoardStateFree>(tile_state)) {
                seen_free = true;
            } else if (std::holds_alternative<TileBoardStateWon>(tile_state)) {
                auto player = std::get<TileBoardStateWon>(tile_state).player;
                if (player == Cross) {
                    seen_cross = true;
                } else {
                    seen_circle = true;
                }
            }
            if (seen_cross && seen_circle) {
                // does not matter if there is a free tile, it is a draw anyway
                this->board_state = TileBoardStateDraw();
                return;
            }
        }
        if (seen_cross && !seen_free) {
            this->board_state = TileBoardStateWon{Cross};
            return;
        }
        if (seen_circle && !seen_free) {
            this->board_state = TileBoardStateWon{Circle};
            return;
        }
    }
}*/

template<typename Tile>
requires TileTrait<Tile>
void GenericBoard<Tile>::update_super_state(GenericPos pos) {
  for (auto line: all_lines_through_point(pos)) {
    auto line_state = LineState{LineStateFree{}};
    for (auto position: all_pos_on_line(line)) {
      line_state = combine_line_states(
        line_state,
        line_state_from_tile_board_state(this->tile(position).state_as_tile()));
    }
    this->line(line) = line_state;
    if (std::holds_alternative<LineStateWon>(line_state)) {
      this->board_state = TileBoardStateWon{std::get<LineStateWon>(line_state).player};
    }
  }
  auto all_l = all_lines();
  if (std::all_of(all_l.begin(),
                  all_l.end(),
                  [&](auto line) { return is_drawn(this->line(line)); })) {
    this->board_state = TileBoardStateDraw{};
  }
}


using TrivialBoard = GenericBoard<TrivialTile>;
using OuterBoard = GenericBoard<TrivialBoard>;

#endif // UTTT_BOARD_HPP
