#ifndef PROJECT_COMMON_HPP
#define PROJECT_COMMON_HPP

#include <array>
#include <iostream>
#include <random>

#include <sockpp/tcp_acceptor.h>
#include <sockpp/tcp_connector.h>
#include <sockpp/tcp_socket.h>
#include <variant>

enum Player {
  Cross = 0,
  Circle = 1,
};

Player random_player_symbol();

struct TileBoardStateFree {
};
struct TileBoardStateWon {
  Player player;
};
struct TileBoardStateDraw {
};

using TileBoardState =
  std::variant<
    TileBoardStateFree,
    TileBoardStateWon,
    TileBoardStateDraw
  >;

#endif // PROJECT_COMMON_HPP
