#include "server.hpp"
#include <variant>

#undef MODE
#define MODE 0

void Server::start_server() {
  std::string Ivp4, port;
  std::cout << "Choose your IPv4 address: ";
  std::getline(std::cin, Ivp4);
  if(Ivp4.empty()) {
    Ivp4 = "localhost";
  }
  std::cout << "IPv4: " << Ivp4 << std::endl;
  std::cout << "Choose your port: ";
  std::getline(std::cin, port);
  if(port.empty()) {
    port = "42069";
  }
  std::cout << "port: " << port << std::endl;
  sockpp::tcp_acceptor acceptor(sockpp::inet_address(Ivp4, std::stoi(port)));
  
  std::cout << "Server is running on " << acceptor.address() << std::endl;

  auto symbol0 = random_player_symbol();
  auto symbol1 = Player(1 - (int) symbol0);

  ServerMessage symbol_assignment;
  json j;

  sockpp::tcp_socket socket0 = acceptor.accept();
  while (true) {
    if (socket0) {
      #if MODE > 0
        std::cout << "Client 0 is connected: " << socket0.peer_address() << std::endl;
      #endif
      symbol_assignment.specific = SymbolAssignment{
        symbol0
      };

      j = symbol_assignment;
      break;
    }
    else {
      #if MODE > 0
        std::cerr << "Error: client 0 is not connected" << std::endl;
      #endif
    }
    socket0 = acceptor.accept();
  }

  std::string tmp0 = j.dump();
  const char *message0 = tmp0.c_str();
  this->sockets[(unsigned) symbol0] = std::move(socket0);
  this->sockets[(unsigned) symbol0].write_n(message0, std::strlen(message0));
  #if MODE > 0
    std::cout << "Connection message sent to client 0" << std::endl;
  #endif

  sockpp::tcp_socket socket1 = acceptor.accept();
  while (true) {
    if (socket1) {
      #if MODE > 0
        std::cout << "Client 1 is connected: " << socket1.peer_address() << std::endl;
      #endif
      symbol_assignment.specific = SymbolAssignment{
        symbol1
      };

      j = symbol_assignment;
      break;
    }
    else {
      #if MODE > 0
        std::cerr << "Error: client 1 is not connected" << std::endl;
      #endif
    }
    socket1 = acceptor.accept();
  }

  std::string tmp1 = j.dump();
  const char *message1 = tmp1.c_str();
  this->sockets[(unsigned) symbol1] = std::move(socket1);
  this->sockets[(unsigned) symbol1].write_n(message1, std::strlen(message1));
  #if MODE > 0
    std::cout << "Connection message sent to client 1" << std::endl;
  #endif
}

void Server::start_sever_with_Ip_port(std::string &Ivp4, int port) {
  sockpp::tcp_acceptor acceptor(sockpp::inet_address(Ivp4, port));
  std::cout << "Server is running on " << acceptor.address() << std::endl;

  auto symbol0 = random_player_symbol();
  auto symbol1 = Player(1 - (int) symbol0);

  ServerMessage symbol_assignment;
  json j;

  sockpp::tcp_socket socket0 = acceptor.accept();
  while (true) {
    if (socket0) {
      #if MODE > 0
        std::cout << "Client 0 is connected: " << socket0.peer_address() << std::endl;
      #endif
      symbol_assignment.specific = SymbolAssignment{
        symbol0
      };

      j = symbol_assignment;
      break;
    }
    else {
      #if MODE > 0
        std::cerr << "Error: client 0 is not connected" << std::endl;
      #endif
    }
    socket0 = acceptor.accept();
  }

  std::string tmp0 = j.dump();
  const char *message0 = tmp0.c_str();
  this->sockets[(unsigned) symbol0] = std::move(socket0);
  this->sockets[(unsigned) symbol0].write_n(message0, std::strlen(message0));
  #if MODE > 0
    std::cout << "Connection message sent to client 0" << std::endl;
  #endif

  sockpp::tcp_socket socket1 = acceptor.accept();
  while (true) {
    if (socket1) {
      #if MODE > 0
        std::cout << "Client 1 is connected: " << socket1.peer_address() << std::endl;
      #endif
      symbol_assignment.specific = SymbolAssignment{
        symbol1
      };

      j = symbol_assignment;
      break;
    }
    else {
      #if MODE > 0
        std::cerr << "Error: client 1 is not connected" << std::endl;
      #endif
    }
    socket1 = acceptor.accept();
  }

  std::string tmp1 = j.dump();
  const char *message1 = tmp1.c_str();
  this->sockets[(unsigned) symbol1] = std::move(socket1);
  this->sockets[(unsigned) symbol1].write_n(message1, std::strlen(message1));
  #if MODE > 0
    std::cout << "Connection message sent to client 1" << std::endl;
  #endif
}
bool Server::play_game() {
  ServerMessage message_from_server;
  ClientMessage message_from_client;

  this->game.start_new_game();
  #if MODE > 0
    std::cout << "Game started" << std::endl;
  #endif
  
  message_from_server.specific = GameStart{
    this->game.data_->currentPlayer
  };
  send_message_to_client(this->sockets[0], message_from_server);
  #if MODE > 0
    std::cout << "Game start message sent to client 0: " << this->sockets[0].peer_address() << std::endl;
  #endif
  send_message_to_client(this->sockets[1], message_from_server);
  #if MODE > 0
    std::cout << "Game start message sent to client 1: " << this->sockets[1].peer_address() << std::endl;
  #endif
  this->sockets[0].set_non_blocking(true);
  this->sockets[1].set_non_blocking(true);
  bool cur_sock = 0;
  while (this->game.state_ != GameState::Finished){
    unsigned cur_player = (unsigned) this->game.data_->currentPlayer;
    //receive_message_from_client(this->sockets[(unsigned) this->game.data_->currentPlayer], message_from_client, &this->buffer[0]);
    if(receive_message_from_client_non_blocking(this->sockets[cur_sock], message_from_client, &this->buffer[0])){
      if(cur_sock == cur_player){
        #if MODE > 0
          std::cout << "Message received from client: "
                    << this->sockets[(unsigned) this->game.data_->currentPlayer].peer_address()
                    << std::endl;
        #endif

        if(std::holds_alternative<PlaceSymbolProposal>(message_from_client.specific)){
          auto proposal_pos = get_place_symbol_proposal(message_from_client);
          auto move = NextMove{
            this->game.data_->currentPlayer,
            get_place_symbol_proposal(message_from_client)

          };
          auto result = this->game.play_move(move);

          if(std::holds_alternative<WrongOuterPosition>(result) ||
             std::holds_alternative<WrongPlayer>(result) ||
             std::holds_alternative<OccupiedPosition>(result)) {
            message_from_server.specific = PlaceSymbolRejected{};
            send_message_to_client(this->sockets[(unsigned) this->game.data_->currentPlayer],
                                   message_from_server);
            #if MODE > 0
              std::cout << "Place symbol rejected message sent to client: "
                        << this->sockets[(unsigned) this->game.data_->currentPlayer].peer_address()
                        << std::endl;
            #endif
          }
          else if(std::holds_alternative<AcceptedWaitForNextMove>(result)) {
            u16 inner_board_idx = proposal_pos.front().linear_idx();
            u16 tile_idx = proposal_pos.back().linear_idx();
            message_from_server.specific = PlaceSymbolAccepted{
              inner_board_idx,
              tile_idx
            };

            send_message_to_client(this->sockets[0],
                                   message_from_server);
            send_message_to_client(this->sockets[1],
                                   message_from_server);
            if(this->game.data_->currentPlayer == Player::Cross) {
              #if MODE > 0
                std::cout << "Move played by: "
                          << "Cross"
                          << std::endl;
              #endif
            }
            else {
              #if MODE > 0
                std::cout << "Move played by: "
                          << "Circle"
                          << std::endl;
              #endif
            }
          }
          else if(std::holds_alternative<Win>(result)) {
            u16 inner_board_idx = proposal_pos.front().linear_idx();
            u16 tile_idx = proposal_pos.back().linear_idx();
            message_from_server.specific = PlaceSymbolAccepted{
              inner_board_idx,
              tile_idx
            };
            #if MODE > 0
              std::cout << "Win message sent to client: "
                        << this->sockets[(unsigned) this->game.data_->currentPlayer].peer_address()
                        << std::endl;
            #endif
            send_message_to_client(this->sockets[0],
                                   message_from_server);
            send_message_to_client(this->sockets[1],
                                   message_from_server);
            if(this->game.data_->currentPlayer == Player::Cross) {
              #if MODE > 0
                std::cout << "Move played by: "
                          << "Cross"
                          << std::endl;
              #endif
            }
            else {
              #if MODE > 0
                std::cout << "Move played by: "
                          << "Circle"
                          << std::endl;
              #endif
            }

          }//TODO: send win message to both clients and end game or start new game
          else if(std::holds_alternative<Draw>(result)) {
            u16 inner_board_idx = proposal_pos.front().linear_idx();
            u16 tile_idx = proposal_pos.back().linear_idx();
            message_from_server.specific = PlaceSymbolAccepted{
              inner_board_idx,
              tile_idx
            };
            #if MODE > 0
              std::cout << "Win message sent to client: "
                        << this->sockets[(unsigned) this->game.data_->currentPlayer].peer_address()
                        << std::endl;
            #endif
            send_message_to_client(this->sockets[0],
                                   message_from_server);
            send_message_to_client(this->sockets[1],
                                   message_from_server);
            if(this->game.data_->currentPlayer == Player::Cross) {
              #if MODE > 0
                std::cout << "Move played by: "
                          << "Cross"
                          << std::endl;
              #endif
            }
            else {
              #if MODE > 0
                std::cout << "Move played by: "
                          << "Circle"
                          << std::endl;
              #endif
            }
            

          }//TODO: send draw message to both clients and end game or start new game
        }else if(std::holds_alternative<OpponentLeft>(message_from_client.specific)){
          //if the player leaves whos turn it is, nice, else, the other player does not get informed
          //until he has made a move... peanuts but annoying;)->could be solved by setting the sockets
          //non blocking for the whole runtime, but i dont know if thats necessary
          message_from_server.specific = OpponentLeft{};
          send_message_to_client(this->sockets[(cur_player + 1)%2],
                                   message_from_server);
          this->game.resign(this->game.current_player());
          this->sockets[0].set_non_blocking(false);
          this->sockets[1].set_non_blocking(false);
          return false;

        }else if(std::holds_alternative<OpponentResigned>(message_from_client.specific)){
          //same problem as with leaving, here even more annoying...
          message_from_server.specific = OpponentResigned{};
          send_message_to_client(this->sockets[0],
                                   message_from_server);
          send_message_to_client(this->sockets[1],
                                   message_from_server);
          this->game.resign(this->game.current_player());
        }
        else {
          #if MODE > 0
            std::cout << "invalid message during game:" << json(message_from_client).dump()  << std::endl;
          #endif
          throw std::runtime_error("Invalid client message during game");
        }
      }else{
        if(std::holds_alternative<OpponentLeft>(message_from_client.specific)){ // only message that should be sent from other client
          //if the player leaves whos turn it is, nice, else, the other player does not get informed
          //until he has made a move... peanuts but annoying;)->could be solved by setting the sockets
          //non blocking for the whole runtime, but i dont know if thats necessary
          message_from_server.specific = OpponentLeft{};
          send_message_to_client(this->sockets[!cur_sock],
                                   message_from_server);
          this->game.resign(this->game.current_player());
          this->sockets[0].set_non_blocking(false);
          this->sockets[1].set_non_blocking(false);
          return false;
        }else {
          #if MODE > 0
            std::cout << "invalid message during game:" << json(message_from_client).dump()  << std::endl;
          #endif
          throw std::runtime_error("Invalid client message during game");
        }
      }
    }
    cur_sock = !cur_sock;
  }
  return this->end_of_game(message_from_client);
}
bool Server::end_of_game(ClientMessage &message_from_client){
  //we dont know which player proposes a revanche first, so we need to read from both sockets, 
  //without beeing held up by one that doesent send anything.
  ServerMessage message_from_server;
  this->sockets[0].set_non_blocking(true);
  this->sockets[1].set_non_blocking(true);
  bool newgame[2] = {false, false};
  bool cur_socket = 1;
  while(!std::holds_alternative<OpponentLeft>(message_from_client.specific)){
    //std::cout << "after game\n";
    bool recieved_msg = receive_message_from_client_non_blocking(this->sockets[cur_socket], message_from_client, &this->buffer[0]);
    //if(std::holds_alternative<RevancheProposal>(message_from_client.specific)){continue;}
    if(recieved_msg && !(std::holds_alternative<PlaceSymbolProposal>(message_from_client.specific) ||
                         std::holds_alternative<OpponentResigned>(message_from_client.specific))){ //no need for those messages anymore
      #if MODE > 0
        std::cout << "Message received from client " << cur_socket << ": "
                  << this->sockets[0].peer_address()
                  << std::endl;
      #endif
      if(std::holds_alternative<RevancheProposal>(message_from_client.specific)){
        message_from_server.specific = RevancheProposal{};
        send_message_to_client(this->sockets[!cur_socket],
                                 message_from_server);
        newgame[cur_socket] = true;
        if(newgame[!cur_socket]){//in case both clients send proposal at same time
          this->sockets[0].set_non_blocking(false);
          this->sockets[1].set_non_blocking(false);
          return true;
        }
      }else if(newgame[!cur_socket] && std::holds_alternative<RevancheAccepted>(message_from_client.specific)){
        //one client proposed, the other accepted->start new game by returning true
        //i guess one revanche message would be sufficient, but more possibilities for messages
        //dont affect performance, right? 
        message_from_server.specific = RevancheAccepted{};
        send_message_to_client(this->sockets[!cur_socket],
                                 message_from_server);
        this->sockets[0].set_non_blocking(false);
        this->sockets[1].set_non_blocking(false);
        return true;
      }else if(std::holds_alternative<OpponentLeft>(message_from_client.specific)){
      //if the player leaves whos turn it is, nice, else, the other player does not get informed
      //until he has made a move... peanuts but annoying;)->could be solved by setting the sockets
      //non blocking for the whole runtime, but i dont know if thats necessary
      message_from_server.specific = OpponentLeft{};
      send_message_to_client(this->sockets[(cur_socket + 1)%2],
                               message_from_server);
      this->sockets[0].set_non_blocking(false);
      this->sockets[1].set_non_blocking(false);
      return false;

    }
      else {
        #if MODE > 0
          std::cout << "invalid message after game:" << json(message_from_client).dump()  << std::endl;
        #endif
        throw std::runtime_error("Invalid client message after game");
      }
    }
    cur_socket = !cur_socket;
  }//one client left-> terminate server by returning false
  message_from_server.specific = OpponentLeft{};
      send_message_to_client(this->sockets[0],
                               message_from_server);
      send_message_to_client(this->sockets[1],
                               message_from_server);
  this->sockets[0].set_non_blocking(false);
  this->sockets[1].set_non_blocking(false);
  return false;
}

void Server::run() {
  this->start_server();
  while(this->play_game()){};//play game(s)
}

void Server::run_with_Ip_port(std::string &Ivp4, int port) {
  this->start_sever_with_Ip_port(Ivp4, port);
  while(this->play_game()){};//play game(s)


}
