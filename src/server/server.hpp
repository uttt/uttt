#ifndef UTTT_SERVER_HPP
#define UTTT_SERVER_HPP

#include "../common/board.hpp"
#include "../common/message.hpp"
#include "../common/game.h"

struct Server {
  private:
  std::array<sockpp::tcp_socket, 2> sockets;
  GameStateMachine game;

  void start_server();
  void start_sever_with_Ip_port(std::string &Ivp4, int port);
  bool play_game();
  bool end_of_game( ClientMessage &message);

  char buffer[1024];

  public:
  void run();
  void run_with_Ip_port(std::string &Ivp4, int port);
};



#endif //UTTT_SERVER_HPP
