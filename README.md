
# UTTT

## Introduction
UTTT is a versatile application built using modern C++ practices. It is designed to run on recent Ubuntu versions and relies on various dependencies managed through Git submodules and CMake.

## Other cool UTTT-applications
If you want to look at another implementation of this game in Rust, give [this](https://gitlab.ethz.ch/uttt/uttt-rs) application a try:


## Prerequisites
Before setting up UTTT, ensure you have the following installed on your system:
- Git
- CMake (version 3.22 or higher)
- A C++ compiler supporting C++20 (e.g., GCC or Clang)
- OpenGL libraries
- GLFW (for window management and OpenGL context creation)
- [Optional] An IDE like CLion for easier project management

## Setting Up

### Cloning the Repository
Clone the repository using SSH:

```sh
git clone git@gitlab.ethz.ch:uttt/uttt.git
cd uttt
```

### Initializing Submodules
After cloning, initialize and update the submodules (dependencies):

```sh
git submodule init
git submodule update
```

### Installing Additional Dependencies
Some dependencies might need to be installed manually. On Ubuntu, you can install them using the following commands:

```sh
sudo apt-get update
sudo apt-get install libglu1-mesa-dev freeglut3-dev mesa-common-dev libglfw3-dev
```

### Building the Project with CMake
You can build the project using CMake directly from the terminal or via an IDE like CLion. To build a release version from the terminal:

```sh
rm -rf build
mkdir build
cmake -B build
make -C build
```

### Additional Notes
- CMake exports compile commands, which means language servers like clangd should work seamlessly with the project.
- Ensure OpenGL and GLFW are correctly installed and configured on your system, as they are required for the UTTT client.


## Running the Game

After successfully building the project, the executable files for the game will be located in the `build` directory.

### Starting the Game
To start a game session, each player needs to run their own instance of the client. It is also possible to play the game with only one client. To do this, navigate to the `build` directory and execute the `uttt_client` executable.

Open a new terminal window for each player and run the following command:

```sh
./build/uttt_client
```

This command starts the UTTT client, and each player should now be able to interact with the game from their respective clients.

## Testing
UTTT includes unit tests, which you can run to verify the correctness of the code:

```sh
./build/uttt_tests
```

## Contributing
We welcome contributions! Please follow the standard Git workflow - fork the repository, make your changes, and submit a pull request.
