#include "gtest/gtest.h"
#include "common/test_simple_board.cpp"
#include "common/test_two_layer_board.cpp"
#include "common/test_entire_game.cpp"
#include "common/test_position.cpp"
#include "common/test_messages.cpp"

// entry point for unit tests, it collects all tests and runs them
int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


