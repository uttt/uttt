#include <gtest/gtest.h>
#include <queue>

#include "../../src/common/message.hpp"


TEST(SerializeDeserializeTest, SymbolAssignment) {
  ServerMessage symbol_assignment_message;
  symbol_assignment_message.specific = SymbolAssignment{Player::Cross};
  json j = symbol_assignment_message;
  std::string message = j.dump();
  EXPECT_EQ(message, "{\"symbol\":0}");

  ServerMessage deserialized_message;
  json j2 = json::parse(message);
  deserialized_message = j2.get<ServerMessage>();
  EXPECT_EQ(std::holds_alternative<SymbolAssignment>(deserialized_message.specific), true);
  EXPECT_EQ(get_symbol_assignment(deserialized_message), Player::Cross);
}

TEST(SerializeDeserializeTest, GameStart) {
  ServerMessage game_start_message;
  game_start_message.specific = GameStart{Player::Cross};
  json j = game_start_message;
  std::string message = j.dump();
  EXPECT_EQ(message, "{\"starting_player\":0}");

  ServerMessage deserialized_message;
  json j2 = json::parse(message);
  deserialized_message = j2.get<ServerMessage>();
  EXPECT_EQ(std::holds_alternative<GameStart>(deserialized_message.specific), true);
  EXPECT_EQ(get_game_start(deserialized_message), Player::Cross);
}

TEST(SerializeDeserializeTest, PlaceSymbolRejected) {
  ServerMessage place_symbol_rejected_message;
  place_symbol_rejected_message.specific = PlaceSymbolRejected{};
  json j = place_symbol_rejected_message;
  std::string message = j.dump();
  EXPECT_EQ(message, "{\"invalid_inner_board_pos\":null}");

  ServerMessage deserialized_message;
  json j2 = json::parse(message);
  deserialized_message = j2.get<ServerMessage>();
  EXPECT_EQ(std::holds_alternative<PlaceSymbolRejected>(deserialized_message.specific), true);
}

TEST(SerializeDeserializeTest, PlaceSymbolAccepted) {
  ServerMessage place_symbol_accepted_message;
  place_symbol_accepted_message.specific = PlaceSymbolAccepted{0, 0};
  json j = place_symbol_accepted_message;
  std::string message = j.dump();
  EXPECT_EQ(message, "{\"inner_board_index\":0,\"tile_index\":0}");

  ServerMessage deserialized_message;
  json j2 = json::parse(message);
  deserialized_message = j2.get<ServerMessage>();
  EXPECT_EQ(std::holds_alternative<PlaceSymbolAccepted>(deserialized_message.specific), true);
  PosIter pos = std::queue<GenericPos>();
  pos.push(GenericPos::fromLinearIdx(0));
  pos.push(GenericPos::fromLinearIdx(0));
  EXPECT_EQ(get_place_symbol_accepted(deserialized_message), pos);
}

TEST(SerializeDeserializeTest, PlaceSymbolProposal) {
  ClientMessage place_symbol_proposal_message;
  place_symbol_proposal_message.specific = PlaceSymbolProposal{0, 0};
  json j = place_symbol_proposal_message;
  std::string message = j.dump();
  EXPECT_EQ(message, "{\"inner_board_index\":0,\"tile_index\":0}");

  ClientMessage deserialized_message;
  json j2 = json::parse(message);
  deserialized_message = j2.get<ClientMessage>();
  EXPECT_EQ(std::holds_alternative<PlaceSymbolProposal>(deserialized_message.specific), true);
  PosIter pos = std::queue<GenericPos>();
  pos.push(GenericPos::fromLinearIdx(0));
  pos.push(GenericPos::fromLinearIdx(0));
  EXPECT_EQ(get_place_symbol_proposal(deserialized_message), pos);
}