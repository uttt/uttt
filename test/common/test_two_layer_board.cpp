#include "gtest/gtest.h"
#include "../../src/common/board.hpp"


namespace TwoLayerBoardTests {

    TEST(TWO_LAYER_BOARD_TESTS, TEST_OUTER_BOARD_INIT) {
        auto board = OuterBoard();
        for (size_t i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            for (size_t j = 0; j < LOCAL_BOARD_SIZE; ++j) {
                auto &tile = board.tile(GenericPos(i, j));
                EXPECT_TRUE(std::holds_alternative<TileBoardStateFree>(tile.state_as_tile()));
                for (size_t k = 0; k < LOCAL_BOARD_SIZE; ++k) {
                    for (size_t l = 0; l < LOCAL_BOARD_SIZE; ++l) {
                        auto pos_iter = PosIter({GenericPos(i, j), GenericPos(k, l)});
                        auto inner_state = board.trivial_tile_in_tile(pos_iter).state_as_tile();
                        EXPECT_TRUE(std::holds_alternative<TileBoardStateFree>(inner_state));
                    }
                }
            }
        }
    }

    TEST(TWO_LAYER_BOARD_TESTS, TEST_OUTER_BOARD_PLACE_SYMBOL) {
        auto symbol = Player::Cross;
        auto board = OuterBoard();
        EXPECT_TRUE(std::holds_alternative<TileBoardStateFree>(board.state_as_board()));
        for (auto i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            for (auto j = 0; j < LOCAL_BOARD_SIZE; ++j) {
                auto outer_pos = GenericPos(i, j);
                for (auto k = 0; k < LOCAL_BOARD_SIZE; ++k) {
                    auto l = 0;
                    auto inner_pos = GenericPos(k, l);
                    auto seq = PosIter ({outer_pos, inner_pos});
                    ASSERT_TRUE(board.place_symbol_in_board(seq, symbol));
                    auto check_seq = PosIter ({outer_pos, inner_pos});
                    auto tile = board.trivial_tile_in_board(check_seq).state_as_tile();
                    EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(tile));
                    EXPECT_EQ(std::get<TileBoardStateWon>(tile).player, symbol);
                }
            }
            EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
            EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, symbol);
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
        EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, symbol);
    }

    TEST(TWO_LAYER_BOARD_TESTS, TEST_OUTER_BOARD_PLACE_SYMBOL_ON_OCCUPIED_TILE) {
        auto board = OuterBoard();
        auto seq = PosIter ({GenericPos(0, 0), GenericPos(0, 0)});
        auto symbol = random_player_symbol();
        EXPECT_TRUE(board.place_symbol_in_board(seq, symbol));
        auto check_seq = PosIter ({GenericPos(0, 0), GenericPos(0, 0)});
        EXPECT_FALSE(board.place_symbol_in_board(check_seq, symbol));
        auto retrieve_seq = PosIter ({GenericPos(0, 0), GenericPos(0, 0)});
        EXPECT_EQ(std::get<TileBoardStateWon>(board.trivial_tile_in_board(retrieve_seq).state_as_tile()).player, symbol);
    }

    TEST(TWO_LAYER_BOARD_TESTS, TEST_CIRCLE_WINS_ALL) {
        const auto outerX = 0, innerX = 0;
        auto board = OuterBoard();
        for (auto outerY = 0; outerY < LOCAL_BOARD_SIZE; ++outerY) {
            auto OuterPos = GenericPos(outerX, outerY);
            for (auto innerY = 0; innerY < LOCAL_BOARD_SIZE; ++innerY) {
                auto innerPos = GenericPos(innerX, innerY);
                auto seq = PosIter ({OuterPos, innerPos});
                ASSERT_TRUE(board.place_symbol_in_board(seq, Player::Circle));
            }
            // check if the outer board is won
            auto outerSeq = GenericPos({OuterPos});
            auto tile = board.tile(outerSeq);
            EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(tile.state_as_tile()));
            EXPECT_EQ(std::get<TileBoardStateWon>(tile.state_as_tile()).player, Player::Circle);
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
        EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, Player::Circle);
    }

    TEST(TWO_LAYER_BOARD_TESTS, TEST_DRAW) {
        auto board = OuterBoard();
        for (auto outerX = 0; outerX < LOCAL_BOARD_SIZE; ++outerX) {
            for (auto outerY = 0; outerY < LOCAL_BOARD_SIZE; ++outerY) {
                auto OuterPos = GenericPos(outerX, outerY);
                Player symbol;
                if (outerX == 0) {
                    symbol = outerY % 2 == 0 ? Player::Circle : Player::Cross;
                } else {
                    symbol = outerY % 2 != 0 ? Player::Circle : Player::Cross;
                }
                for (auto innerX = 0; innerX < LOCAL_BOARD_SIZE; ++innerX) {
                    auto innerPos = GenericPos(innerX, innerX);
                    auto seq = PosIter ({OuterPos, innerPos});
                    ASSERT_TRUE(board.place_symbol_in_board(seq, symbol));
                }
            }
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateDraw>(board.state_as_board()));
    }
}

