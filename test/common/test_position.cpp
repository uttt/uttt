

#include "gtest/gtest.h"
#include "../../src/common/pos.hpp"
#include "../../src/common/constants_.hpp"


namespace PositionTests {

    TEST(PositionTests, TEST_POSITION_INIT) {
        auto pos = GenericPos(2, 1);
        EXPECT_EQ(pos.x, 2);
        EXPECT_EQ(pos.y, 1);
    }

    TEST(PositionTests, TEST_POSITION_INIT_NEGATIVE) {
        EXPECT_THROW(GenericPos(-1, 0), std::runtime_error);
    }

    TEST(PositionTests, TEST_POSITION_INIT_TOO_LARGE) {
        EXPECT_THROW(GenericPos(LOCAL_BOARD_SIZE, 0), std::runtime_error);
    }

    TEST(PositionTests, TEST_LINEAR_INDEX){
        for (auto x = 0; x < LOCAL_BOARD_SIZE; ++x) {
            for (auto y = 0; y < LOCAL_BOARD_SIZE; ++y) {
                auto pos = GenericPos(x, y);
                auto idx = pos.linear_idx();
                auto replicated = GenericPos::fromLinearIdx(idx);
                EXPECT_EQ(pos, replicated);
            }
        }
    }
}

