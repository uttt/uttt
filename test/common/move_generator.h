#ifndef MOVE_GENERATOR_H
#define MOVE_GENERATOR_H


#include "../../src/common/game.h"

using namespace std;

class MoveGenerator {
    // this class is used to generate random moves (for testing)
    // it generates all possible moves in advance and then returns them in somewhat random order
    // it is used to test the game logic
    mt19937 twisterGenerator;
    vector<PosIter> remainingSequences;
    vector<GenericPos> remainingInnerPositions;
public:
    MoveGenerator() {
        random_device rd;
        twisterGenerator = mt19937(rd());
        remainingInnerPositions = vector<GenericPos>();
        remainingSequences = vector<PosIter>();
        reset();
    }

    void reset() {
        remainingInnerPositions.clear();
        remainingSequences.clear();
        for (auto j = 0; j < LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE; ++j) {
            remainingInnerPositions.emplace_back(GenericPos::fromLinearIdx(j));
            auto outerPos = GenericPos::fromLinearIdx(j);
            for (auto i = 0; i < LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE; ++i) {
                remainingSequences.emplace_back(PosIter({outerPos, GenericPos::fromLinearIdx(i)}));
            }
        }
        shuffle(remainingInnerPositions.begin(), remainingInnerPositions.end(), twisterGenerator);
        shuffle(remainingSequences.begin(), remainingSequences.end(), twisterGenerator);
        assert(remainingInnerPositions.size() == LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE);
        assert(remainingSequences.size() == LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE * LOCAL_BOARD_SIZE);
    }

    GenericPos draw_random_pos() {
        assert(!remainingInnerPositions.empty());
        auto pos = remainingInnerPositions.back();
        remainingInnerPositions.pop_back();
        return pos;
    }

    PosIter draw_random_pos_iter() {
        assert(!remainingSequences.empty());
        auto seq = remainingSequences.back();
        remainingSequences.pop_back();
        return seq;
    }
};

#endif //MOVE_GENERATOR_H
