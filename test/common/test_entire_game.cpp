#include "gtest/gtest.h"
#include "../../src/common/game.h"
#include "move_generator.h"


namespace CompleteGameTests {

    TEST(CompleteGameTests, TEST_PLAY_MOVE_WITHOUT_START) {
        auto moves = std::vector<NextMove>();
        auto game = GameStateMachine();
        auto move = NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(0, 0)}));
        auto result = game.play_move(move);
        EXPECT_TRUE(std::holds_alternative<GameNotInProgress>(result));
    }

    TEST(CompleteGameTests, TEST_PLAY_MOVE_AFTER_START) {
        auto moves = std::vector<NextMove>();
        auto game = GameStateMachine(Player::Cross);
        game.start_new_game();
        auto move = NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(1, 2)}));
        auto result = game.play_move(move);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        EXPECT_EQ(std::get<AcceptedWaitForNextMove>(result).player, Player::Circle);
        EXPECT_EQ(std::get<AcceptedWaitForNextMove>(result).outerPosition, GenericPos(1, 2));
    }

    TEST(CompleteGameTests, EST_PLAY_MOVE_AFTER_START_WRONG_PLAYER) {
        auto moves = std::vector<NextMove>();
        auto game = GameStateMachine(Player::Cross);
        game.start_new_game();
        auto move = NextMove(Player::Circle, PosIter({GenericPos(0, 0), GenericPos(0, 0)}));
        auto result = game.play_move(move);
        EXPECT_TRUE(std::holds_alternative<WrongPlayer>(result));
        EXPECT_EQ(std::get<WrongPlayer>(result).expectedPlayer, Player::Cross);
    }

    TEST(CompleteGameTests, TEST_PLAY_MOVE_AFTER_START_WRONG_OUTER_POS) {
        auto game = GameStateMachine(Player::Cross);
        game.start_new_game();
        auto move = NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(1, 1)}));
        auto result = game.play_move(move);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        auto next_move = NextMove(Player::Circle, PosIter({GenericPos(0, 0), GenericPos(0, 0)}));
        auto next_result = game.play_move(next_move);
        EXPECT_TRUE(std::holds_alternative<WrongOuterPosition>(next_result));
        EXPECT_EQ(std::get<WrongOuterPosition>(next_result).expectedPosition, GenericPos(1, 1));
    }

    TEST(CompleteGameTests, TEST_START_AND_RESIGN) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_TRUE(game.start_new_game());
        EXPECT_TRUE(game.resign(Player::Cross));
        EXPECT_EQ(game.state(), GameState::Finished);
        EXPECT_EQ(game.stats().cross_wins, 0);
        EXPECT_EQ(game.stats().circle_wins, 1);
        EXPECT_EQ(game.stats().draws, 0);
    }

    TEST(CompleteGameTests, TEST_START_AND_RESIGN_OTER_PLAYER) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_FALSE(game.resign(Player::Circle));
        EXPECT_TRUE(game.start_new_game());
        EXPECT_TRUE(game.resign(Player::Circle));
        EXPECT_EQ(game.state(), GameState::Finished);
        EXPECT_EQ(game.stats().cross_wins, 1);
        EXPECT_EQ(game.stats().circle_wins, 0);
        EXPECT_EQ(game.stats().draws, 0);
    }

    TEST(CompleteGameTests, TEST_START_AND_RESIGN_TWICE) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_TRUE(game.start_new_game());
        EXPECT_TRUE(game.resign(Player::Cross));
        EXPECT_FALSE(game.resign(Player::Cross));
        EXPECT_EQ(game.state(), GameState::Finished);
        EXPECT_EQ(game.stats().cross_wins, 0);
        EXPECT_EQ(game.stats().circle_wins, 1);
        EXPECT_EQ(game.stats().draws, 0);
    }

    TEST(CompleteGameTests, TEST_START_AND_PLAY_MOVE_AND_RESIGN) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_TRUE(game.start_new_game());
        auto move = NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(0, 0)}));
        auto result = game.play_move(move);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        EXPECT_TRUE(game.resign(Player::Circle));
        EXPECT_EQ(game.state(), GameState::Finished);
        EXPECT_EQ(game.stats().cross_wins, 1);
        EXPECT_EQ(game.stats().circle_wins, 0);
        EXPECT_EQ(game.stats().draws, 0);
    }

    TEST(CompleteGameTests, TEST_WITH_RANDOM_MOVES) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_TRUE(game.start_new_game());
        auto current_player = game.current_player();
        auto move_generator = MoveGenerator();
        move_generator.reset();
        while (true) {
            auto next_move = NextMove(current_player, move_generator.draw_random_pos_iter());
            auto result = game.play_move(next_move);
            if (std::holds_alternative<AcceptedWaitForNextMove>(result)) {
                current_player = std::get<AcceptedWaitForNextMove>(result).player;
                move_generator.reset();
                continue;
            }
            if (std::holds_alternative<Win>(result)) { break; }
            if (std::holds_alternative<Draw>(result)) { break; }
        }
        EXPECT_EQ(game.total_games(), 1);
        EXPECT_EQ(game.state(), GameState::Finished);
    }

    TEST(CompleteGameTests, TEST_CANNOT_PLACE_IN_WON_FIELD) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_TRUE(game.start_new_game());
        auto cross_moves = std::vector<NextMove>{
                NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(1, 0)})),
                NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(2, 0)})),
                NextMove(Player::Cross, PosIter({GenericPos(0, 0), GenericPos(0, 0)}))};
        auto circle_moves = std::vector<NextMove>{
                NextMove(Player::Circle, PosIter({GenericPos(1, 0), GenericPos(0, 0)})),
                NextMove(Player::Circle, PosIter({GenericPos(2, 0), GenericPos(0, 0)}))
        };
        auto result = game.play_move(cross_moves[0]);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        result = game.play_move(circle_moves[0]);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        result = game.play_move(cross_moves[1]);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        result = game.play_move(circle_moves[1]);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        result = game.play_move(cross_moves[2]);
        EXPECT_TRUE(std::holds_alternative<AcceptedWaitForNextMove>(result));
        auto break_move = NextMove(Player::Circle, PosIter({GenericPos(0, 0), GenericPos(0, 2)}));
        result = game.play_move(break_move);
        EXPECT_TRUE(std::holds_alternative<OccupiedPosition>(result));
    }

    TEST(CompleteGameTests, TEST_RESIGN_AFTER_10_MOVES) {
        auto game = GameStateMachine(random_player_symbol());
        EXPECT_TRUE(game.start_new_game());
        auto current_player = game.current_player();
        auto move_generator = MoveGenerator();
        move_generator.reset();
        auto move_count = 0;
        while (true) {
            if (move_count > 10) {
                EXPECT_TRUE(game.resign(current_player));
                break;
            }
            auto next_move = NextMove(current_player, move_generator.draw_random_pos_iter());
            auto result = game.play_move(next_move);
            if (std::holds_alternative<AcceptedWaitForNextMove>(result)) {
                current_player = std::get<AcceptedWaitForNextMove>(result).player;
                move_generator.reset();
                move_count++;
                continue;
            }
        }
        EXPECT_EQ(game.total_games(), 1);
        EXPECT_EQ(game.state(), GameState::Finished);
        auto win_count = current_player == Circle ? game.stats().cross_wins : game.stats().circle_wins;
        EXPECT_EQ(win_count, 1);
    }

    TEST(CompleteGameTests, TEST_THAT_EVERY_OUTCOME_IS_POSSIBLE) {
        auto game = GameStateMachine(Player::Cross);
        EXPECT_TRUE(game.start_new_game());
        auto current_player = game.current_player();
        auto move_generator = MoveGenerator();
        move_generator.reset();
        while (true) {
            game.start_new_game();
            current_player = game.current_player();
            while (true) {
                auto next_move = NextMove(current_player, move_generator.draw_random_pos_iter());
                auto result = game.play_move(next_move);
                if (std::holds_alternative<AcceptedWaitForNextMove>(result)) {
                    current_player = std::get<AcceptedWaitForNextMove>(result).player;
                    move_generator.reset();
                    continue;
                }
                if (std::holds_alternative<Win>(result)) { break; }
                if (std::holds_alternative<Draw>(result)) { break; }
            }
            EXPECT_EQ(game.state(), GameState::Finished);
            if (game.total_games() > 10000) { throw std::runtime_error("Too many games"); }
            if (game.stats().cross_wins > 0 && game.stats().circle_wins > 0 && game.stats().draws > 0) { break; }
        }
        EXPECT_GE(game.total_games(), 3);
        EXPECT_GE(game.stats().cross_wins, 1);
        EXPECT_GE(game.stats().circle_wins, 1);
        EXPECT_GE(game.stats().draws, 1);
    }
}