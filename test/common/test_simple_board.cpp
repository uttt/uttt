


#include "gtest/gtest.h"
#include "../../src/common/common.hpp"
#include "../../src/common/board.hpp"


namespace SimpleBoardTests {

    TEST(SimpleBoardTests, TEST_POS_SEQUENCE) {
        PosIter seq = PosIter ({GenericPos(1, 2), GenericPos(2, 0)});
        EXPECT_TRUE(seq.size() == 2);
        EXPECT_EQ(seq.front().x, 1);
        EXPECT_EQ(seq.front().y, 2);
        EXPECT_FALSE(seq.empty());
        EXPECT_EQ(seq.back().x, 2);
        EXPECT_EQ(seq.back().y, 0);
    }

    TEST(SimpleBoardTests, TEST_BOARD_INIT) {
        // change this test to briefly check all positions check if all positions are initialized as empty
        auto board = TrivialBoard();
        for (auto i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            for (auto j = 0; j < LOCAL_BOARD_SIZE; ++j) {
                auto seq = PosIter ({GenericPos(i, j)});
                auto tile = board.trivial_tile_in_board(seq).state_as_tile();
                EXPECT_TRUE(std::holds_alternative<TileBoardStateFree>(tile));
            }
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateFree>(board.state_as_board()));
    }

    TEST(SimpleBoardTests, TEST_BOARD_PLACE_SYMBOL) {
        for (auto i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            for (auto j = 0; j < LOCAL_BOARD_SIZE; ++j) {
                const auto example_pos = GenericPos(i, j);
                auto symbol = random_player_symbol();
                auto seq = PosIter({example_pos});
                auto board = TrivialBoard();
                auto tile = board.trivial_tile_in_tile(seq).state_as_tile();

                EXPECT_TRUE(std::holds_alternative<TileBoardStateFree>(tile));

                // Place a symbol
                auto placement_seq = PosIter({example_pos});
                EXPECT_TRUE(board.place_symbol_in_board(placement_seq, symbol));
                auto occupied_iter = PosIter({example_pos});
                auto occupied_tile = board.trivial_tile_in_tile(occupied_iter).state_as_tile();
                EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(occupied_tile));
                EXPECT_EQ(std::get<TileBoardStateWon>(occupied_tile).player, symbol);

                // Place a symbol on the same position (should fail)
                auto occupied_seq = PosIter({example_pos});
                EXPECT_FALSE(board.place_symbol_in_board(occupied_seq, symbol));
                // Did not change the symbol
                auto check_iter = PosIter({example_pos});
                auto check = board.trivial_tile_in_tile(check_iter).state_as_tile();
                EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(check));
                EXPECT_EQ(std::get<TileBoardStateWon>(check).player, symbol);
            }
        }
    }
    TEST(SimpleBoardTests, TEST_WIN_A_ROW) {
        auto board = TrivialBoard();
        for (auto i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            auto seq = PosIter({GenericPos(0, i)});
            EXPECT_TRUE(board.place_symbol_in_board(seq, Player::Cross));
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
        EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, Player::Cross);
    }

    TEST(SimpleBoardTests, TEST_WIN_A_COLUMN) {
        auto board = TrivialBoard();
        for (int i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            auto seq = PosIter({GenericPos(i, 0)});
            EXPECT_TRUE(board.place_symbol_in_board(seq, Player::Cross));
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
        EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, Player::Cross);
    }

    TEST(SimpleBoardTests, TEST_WIN_A_DIAGONAL) {
        auto board = TrivialBoard();
        for (int i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            auto seq = PosIter({GenericPos(i, i)});
            EXPECT_TRUE(board.place_symbol_in_board(seq, Player::Cross));
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
        EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, Cross);
    }

    TEST(SimpleBoardTests, TEST_WIN_AN_ANTIDIAGONAL) {
        auto board = TrivialBoard();
        for (int i = 0; i < LOCAL_BOARD_SIZE; ++i) {
            auto seq = PosIter({GenericPos(i, LOCAL_BOARD_SIZE - i - 1)});
            EXPECT_TRUE(board.place_symbol_in_board(seq, Cross));
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateWon>(board.state_as_board()));
        EXPECT_EQ(std::get<TileBoardStateWon>(board.state_as_board()).player, Cross);
    }


    TEST(SimpleBoardTests, TEST_DRAW) {
        auto board = TrivialBoard();
        for (auto x = 0; x < LOCAL_BOARD_SIZE; ++x) {
            for (auto y = 0; y < LOCAL_BOARD_SIZE; ++y) {
                Player symbol;
                if (x == 0) {
                    symbol = y % 2 == 0 ? Circle : Cross;
                } else {
                    symbol = y % 2 != 0 ? Circle : Cross;
                }
                auto seq = PosIter({GenericPos(x, y)});
                ASSERT_TRUE(board.place_symbol_in_tile(seq, symbol));
            }
        }
        EXPECT_TRUE(std::holds_alternative<TileBoardStateDraw>(board.state_as_board()));
    }
}


